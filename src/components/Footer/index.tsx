import { useEffect, useState } from "react";
import Link from 'next/link'

import { Alert, Grid, Snackbar, Button } from "@mui/material";
import * as gtag from "../../utils/gtag";

import { Container } from "../../styles/global";
import { PoliticaDePrivacidadeModal } from "../PoliticaDePrivacidadeModal";

import { Footer, ContainerFooter } from "./style";

interface FooterApplicationProps {
  information: boolean;
  showLogos: boolean;
}

export default function FooterAplication({
  information,
  showLogos,
}: FooterApplicationProps) {
  const [open, setOpen] = useState(false);
  const [openAlert, setOpenAlert] = useState(true);

  useEffect(() => {
    getPrivacyPolicy();
  }, []);

  const handleClose = () => setOpen(false);

  const handleAccept = () => {
    localStorage.setItem("privacy-policy-we-sorocaba", "accept");
    setOpenAlert(false);
  };

  const getPrivacyPolicy = () => {
    const value = localStorage.getItem("privacy-policy-we-sorocaba");
    if (value) {
      setOpenAlert(false);
    }
  };

  const handleClickWhatsApp = () => {
    gtag.event({ action: "Clique", category: 'Contato', label: 'whatsapp' });
    
    window.open(
      "https://api.whatsapp.com/send?phone=5521965470005&text=Ol%C3%A1%2C%20gostaria%20de%20saber%20mais%20informa%C3%A7%C3%B5es%20sobre%20o%20We%20Sorocaba",
      "_blank"
    );
  };

  return (
    <Footer>
      <Container>
        {showLogos && (
          <Grid container marginBottom={!information ? 2 : 0}>
            <Grid item xs={6} className="containerLeft">
              <img
                src="/images/logo-gafisa.png"
                alt="Logotipo da Gafisa."
              />
            </Grid>
            <Grid item xs={6} className="containerRight">
              <img
                src="/images/logo-viver-bem.png"
                alt="Logotipo da Gafisa."
              />
            </Grid>
            {information && (
              <Grid
                item
                xs={12}
                textAlign="center"
                marginTop={5}
                marginBottom={3}
              >
                <p>
                  *Apartamentos 103, 104 e 204 com 1 vaga e cobertura 701, 702 e
                  703 com 3 vagas.
                </p>
              </Grid>
            )}
          </Grid>
        )}
      </Container>
      <ContainerFooter>
        <Grid container>
          <Grid
            item
            sm={6}
            xs={12}
            display="flex"
            alignItems="center"
            className="policy"
          >
            <Link href="/politica-de-privacidade">
              <p>
                Direitos reservados a Gafisa. Acesse a nossa{" "}
                <strong
                  // onClick={() => setOpen(true)}
                  style={{ marginLeft: ".5rem", cursor: "pointer" }}
                >
                  política de privacidade.
                </strong>
              </p>
            </Link>
          </Grid>
          <Grid
            item
            sm={6}
            xs={12}
            display="flex"
            alignItems="center"
            className="copyright"
          >
            <p>
              Desenvolvido por{" "}
              <a
                href="https://internit.com.br/"
                target="_blank"
                rel="noreferrer"
              >
                <img
                  src="/images/logo-internit.png"
                  width="100%"
                  alt="Logo da internit"
                />
              </a>
              <a
                href="https://incenadigital.com.br/"
                target="_blank"
                rel="noreferrer"
              >
                <img
                  src="/images/logo-incena.png"
                  width="70%"
                  alt="Logo Incena Digital"
                  className="incena"
                />
              </a>
            </p>
          </Grid>
        </Grid>

        <div className="whatsapp">
          <span onClick={handleClickWhatsApp} id="button-whatsapp">
            <img
              src="/images/whatsapp.png"
              alt="Botão de WhatsApp"
            />
          </span>
        </div>
      </ContainerFooter>
      <PoliticaDePrivacidadeModal open={open} handleClose={handleClose} />
      <Snackbar
        open={openAlert}
        sx={{ width: "40%" }}
        className="popUpPrivacyPolicy"
      >
        <Alert severity="info">
          Para melhorar a sua experiência de navegação, utilizamos de cookies,
          entre outras tecnologias. De acordo com a nossa{" "}
          <strong onClick={() => setOpen(true)}>Política de Privacidade</strong>{" "}
          , ao continuar navegando, você aceita estas condições. Acesse nossa{" "}
          <strong onClick={() => setOpen(true)}>Política de Privacidade</strong>{" "}
          e confira como tratamos os dados pessoais aqui na Gafisa.
          <br />
          <Button onClick={handleAccept}>Concordar e continuar</Button>
        </Alert>
      </Snackbar>
    </Footer>
  );
}
