import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from "@mui/material";

interface PoliticaDePrivacidadeProps {
  open: boolean;
  handleClose: () => void;
}

export const PoliticaDePrivacidadeModal = ({
  open,
  handleClose,
}: PoliticaDePrivacidadeProps) => {
  return (
    <Dialog
      open={open}
      keepMounted
      onClose={handleClose}
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogTitle>Política de Privacidade</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-slide-description">
          <p>
            Faz a divulgação dessa política comprovando a obrigação da companhia
            com a privacidade e proteção de dados de quem interage em nossas
            plataformas, aplicativos, sites, etc. além de estabelecer as regras
            destes tratamentos. as referências desta política de privacidade têm
            como base a - lei nº 13.709/18 - lei geral de proteção de dados
            pessoais.
          </p>
          <h3 style={{ marginTop: '1rem' }}>Finalidade</h3>
          <p>
            A Gafisa, em linha com as melhores práticas de mercados e com o
            quanto disposto na Lei Geral de Proteção de Dados, expressa o seu
            comprometimento na adoção de medidas para tutelar as informações
            fornecidas por seus Clientes e demais usuários nos meios digitais,
            visando proteger os direitos fundamentais de liberdade e
            privacidade. Dessa maneira, institui a Política de Privacidade da
            Gafisa (“Política”).
          </p>
          <p>
            A Política tem por finalidade estabelecer regras e procedimentos
            para a coleta, tratamento e guarda de informações e/ou dados
            pessoais obtidos no exercício de suas atividades empresariais,
            aplicáveis, também, às demais empresas integrantes do Grupo
            Econômico da Gafisa, especificamente, aqueles coletados no Site,
            aplicativos e demais sistemas interativos disponíveis na Internet e
            utilizados pela Gafisa.
          </p>
          <p>
            Nosso compromisso é manter as informações coletadas de forma
            sigilosa e visando sempre utilizá-las no interesse do usuário e/ou
            Cliente da Gafisa, podendo, para tanto, ofertar produtos ou
            serviços, inclusive oportunidades de parceiros comerciais da Gafisa
            aptos a otimizar o processo de aquisição do imóvel, ou, até mesmo,
            deixa-lo com o jeito tão desejado pelo seu Cliente ou usuário do
            Site. Os termos, adiante mencionados, e disposições previstas na
            Política estão em linha com o quanto disposto na Lei nº 13.709/18 –
            Lei Geral de Proteção de Dados e demais normativos protetivos de
            dados pessoais, nacionais e internacionais.
          </p>
          <h2 style={{ marginTop: '1rem' }}>Coleta de Dados</h2>
          <h3 style={{ marginTop: '1rem' }}>Como é realizada a Coleta de Dados?</h3>
          <p>
            A coleta de dados pessoais e demais informações ocorre ao acessar o
            site ou demais plataformas utilizadas pela Gafisa ou empresas
            integrantes do seu Grupo Econômico. Usualmente, coletamos nome,
            estado civil, profissão, número de documentos, telefones, endereços
            e e-mails, visando a manutenção do contato próximo da Gafisa com o
            usuário ou cliente. Qualquer tipo de dados, inclusive os pessoais,
            podem ser coletados quando um cliente nos visita, seja em nossos
            stands, seja quando interage com nossas plataformas, sites,
            aplicativos ou efetua acordos comerciais.
          </p>
          <h3 style={{ marginTop: '1rem' }}>Tipo de dados e motivo da coleta:</h3>
          <p>
            Atender solicitações dos usuários, melhorar a experiência e divulgar
            nossos produtos e serviços: Nome Completo, CPF, Telefone, e-mail,
            entre outros.
          </p>
          <p>
            Atendimento a solicitações inerentes aquisição de imóveis e demais
            produtos e serviços, avaliação de crédito e cadastro de manifestação
            de interesse em participar de processos seletivos: Dados Bancários;
            Endereço do banco onde adquiriu as ações (caso seja um investidor);
            Dados do Contrato (Fornecedores e Parceiros de Negócios);
            Experiência Profissional (candidato à uma vaga). Identificar e
            resguardar informações de visitantes digitais, cumprir obrigações
            legais e prevenir fraudes ou qualquer outro risco associado a
            navegação na internet: Endereço IP, Porta Lógica de Origem,
            Dispositivo e Versão, Geolocalização, Registros de data e horário de
            ações, Telas acessadas, ID da Sessão e Cookies;
          </p>
          <h3 style={{ marginTop: '1rem' }}>A necessidade do envio de dados:</h3>
          <p>
            A aquisição de nossos produtos e serviços dependem totalmente de
            dados informados, especialmente os dados cadastrais para fins de
            análise de crédito ou definição de perfil de produto e serviço a
            serem ofertados. O não consentimento no uso ou no fornecimento de
            informações poderá limitar o acesso ou até mesmo impossibilitar a
            oferta de produtos e serviços.
          </p>
          <h3 style={{ marginTop: '1rem' }}>Atualização e autenticidade dos dados:</h3>
          <p>
            O único responsável pela precisão, autenticidade ou falta dela em
            relação aos dados ou pela sua desatualização, é você. Portanto,
            lembre-se de garantir a veracidade, atualidade e correção dos dados
            ou informações prestadas.
          </p>
          <p>
            A Gafisa reserva o direito de não realizar o processamento do
            tratamento a informação ou dado coletado, ou até mesmo deixar de
            prestar serviços ou comercialização de produtos, se for verificado a
            existência de erro, omissão ou até mesmo indícios de que o
            tratamento das informações e/ou dados pessoais prestados possa nos
            imputar qualquer infração de qualquer lei aplicável, ou se você
            estiver utilizando a Plataforma para quaisquer fins ilegais,
            ilícitos ou contrários à moralidade.
          </p>
          <p>
            Uma vez prestada a informação e/ou transmitido e coletado o dado, a
            Gafisa passa a ser responsável pelo uso e sua guarda, com a
            finalidade de ofertar produtos ou serviços de interesse de
            adquirentes dos imóveis, inclusive, poderá, por si ou terceiro,
            divulgar serviços e produtos vocacionados a permitir a conclusão da
            aquisição de imóvel, a concessão de empréstimos, itens de decoração
            e personalização de unidades, tudo com o único propósito de sonhar e
            realizar os sonhos de seus clientes e usuários. Ressaltamos que o
            uso, acesso e compartilhamento (apenas quando necessário) com
            terceiros, serão feitos dentro dos limites e propósitos dos negócios
            descritos nesta Política.
          </p>
          <h3 style={{ marginTop: '1rem' }}>COMPARTILHAMENTO DE DADOS E INFORMAÇÕES</h3>
          <p>
            A coleta de dados e as atividades apontadas podem ser compartilhados
            apenas:
          </p>
          <ul>
            <li>
              Com autoridades, sejam elas: judiciais, administrativas ou
              governamentais competentes, havendo determinação legal,
              requerimento, requisição ou ordem judicial;
            </li>
            <li>Para análise e concessão de crédito;</li>
            <li>
              Com instituições na responsabilidade de cobrar o crédito
              decorrente da compra da(s) unidade(s) imobiliária(s) adquirentes;
              Com o consultor (corretor) imobiliário, quando a compra tiver
              intermédio;
            </li>
            <li>Instituições financeiras de financiamento de imóvel;</li>
            <li>
              Administradora do condomínio, a qual oferecerá os serviços de
              gestão e administração;
            </li>
            <li>
              De forma instintiva, em caso de movimentações societárias, como
              fusão, aquisição e incorporação;
            </li>
            <li>
              Para fins de seleção e participação em processo seletivo de
              candidatos a vagas ou posições de trabalho dentro do Grupo Gafisa;
            </li>
            <li>
              Oferta de produtos e serviços, inclusive, itens para decoração e
              personalização dos imóveis vendidos pela Gafisa;
            </li>
            <li>Securitização de recebíveis.</li>
          </ul>
          <h3>A gafisa e seus dados</h3>
          <p>
            A Gafisa é a controladora das informações e dados pessoais coletados
            em seu Site e demais plataformas utilizadas por empresas integrantes
            de seu Grupo Econômico, sendo que o tratamento das informações e
            dados coletados poderá ser realizado pela Gafisa ou outras empresas
            integrantes de seu Grupo Econômico ou por terceiros contratados pela
            Gafisa para a finalidade prevista na Política.
          </p>
          <p>
            Caso venha a receber comunicação de terceiros não desejada ou queira
            certificar-se da utilização de acordo com a finalidade de Política,
            ou requerer, após 15 dias contados do envio da solicitação, a
            exclusão ou a não utilização das informações ou dados pessoais pela
            Gafisa ou terceiros por ela contratados, como por exemplo
            fabricantes de móveis, eletrodomésticos e etc,.solicitamos que seja
            realizado o contato conosco por meio dos Canais de Atendimento
            disponíveis ou pelo e-mail: protecaodados@gafisa.com.br.
          </p>
          <p>
            Durante o acesso ao Site ou demais plataformas digitais utilizadas
            pela Gafisa ou empresas integrantes de seu Grupo Econômico, a Gafisa
            poderá coletar:
          </p>
          <ul>
            <li>
              O IP da máquina durante a navegação do cliente e/ou usuários, a
              fim de garantir o monitoramento e demais medidas de segurança,
              visando proteger os dados, informações e integridade de seu banco
              de dados;
            </li>
            <li>
              Localização atual do usuário com a finalidade de dar o tratamento
              adequado aos dados ou informações coletadas de estrangeiros ou
              para fins de estudo de perfil ou área de interesse de produtos e
              serviços;
            </li>
            <li>
              Cookies arquivos para rastrear movimentos dentro dos sites,
              podendo o usuário ou cliente recursar o seu uso durante a
              navegação ou, a qualquer tempo, bloquear o uso por meio de
              ferramentas de bloqueio existentes nos principais navegadores de
              internet.
            </li>
          </ul>
          <h2 style={{ marginTop: '1rem' }}>Saiba como proteger seus dados:</h2>
          <p>
            NÃO DIVULGUE NEM COMPARTILHE SUA SENHA: você também é responsável
            pelo sigilo de seus dados pessoais e deve ter sempre ciência de que
            o compartilhamento de senhas e dados de acesso viola esta Política e
            pode comprometer a segurança dos seus dados e da Plataforma. É
            essencial que você proteja seus dados contra acesso não autorizado
            ao seu computador, além de se assegurar de sempre clicar em “sair”
            ao finalizar sua navegação em algum equipamento compartilhado.
          </p>
          <p>
            A Gafisa repudia e não realiza o envio de mensagens eletrônicas
            maliciosas, contendo solicitação de confirmação de dados ou com
            anexos que possam ser executados (extensões: .exe, .com, entre
            outros) ou links para eventuais downloads.
          </p>
          <p>
            LINKS EXTERNOS: quando nossa plataforma é utilizada, ela poderá
            conduzi-lo via link a outros portais ou plataformas, que poderão
            coletar suas informações e ter sua própria Política de Tratamento de
            Dados. É de sua responsabilidade consentir com o uso das informações
            e dados coletados, bem como realizar a leitura das Políticas de
            Privacidade de tais portais ou plataformas fora do nosso ambiente da
            Gafisa.
          </p>
          <p>
            A Gafisa ou qualquer empresa integrante de seu Grupo Econômico não é
            responsável pelo controle e tratamento de dados fora de seu website,
            muito menos de seu conteúdo ou Políticas de Privacidade de empresas
            não integrantes do Grupo Econômico da Gafisa, sendo o titular do
            website o único responsável pela coleta, tratamento, guarda e
            monitoramento da correta utilização com base na finalidade contida
            no consentimento expresso no momento da coleta em website de
            terceiro não integrante ao Grupo Econômico da Gafisa.
          </p>
          <p>
            SERVIÇOS E PARCERIAS: Com a finalidade de atender as necessidades de
            seus clientes, a Gafisa possui acordos comerciais com terceiros que
            poderão oferecer serviços por meio de funcionalidades ou sites que
            podem a partir do website ou plataforma da Gafisa As informações e
            dados coletados diretamente no website de nosso parceiro, mediante o
            consentimento emitido por você, são de responsabilidade exclusiva do
            parceiro sujeitando-se às práticas próprias previstas em suas
            Políticas de Privacidade.
          </p>
          <p>
            PROCESSAMENTO POR TERCEIROS SOB NOSSA DIRETRIZ: A Gafisa poderá
            contratar ou estabelecer relações comerciais com tratamento de
            informações ou dados pessoais com terceiros, que deverão manter a
            confidencialidade da informação e/ou dados compartilhados pela
            Gafisa na qualidade de controladora da informação ou dado coletado,
            bem como deverão respeitar a Política, sob pena de sanções
            contratuais ou demais medidas a serem adotadas pela Gafisa no
            interesse de manter o uso da informação e dado aderente ao
            consentimento manifestado e para a finalidade de sua coleta e
            utilização.
          </p>
          <p>
            As empresas contratadas pela Gafisa para realizar o tratamento das
            informações ou dados deverão possuir a mesmas estrutura e regras de
            segurança da informação aplicada no âmbito do Grupo Gafisa. FALE
            CONOSCO: Buscamos sempre o melhor para nossos clientes e futuros
            clientes e não queremos que nenhuma dúvida fique em aberto,
            portanto, com o intuito de otimizar e melhorar nossa comunicação,
            quando enviamos um e-mail para você, podemos receber uma notificação
            quando eles são abertos, desde que esta possibilidade esteja
            disponível. É importante você ficar atento, pois os e-mails são
            enviados somente pelos domínios: @gafisa.com.br;
            @gafisavendas.com.br.
          </p>
          <p>
            OMO É REALIZADO O ARMAZENAMENTO DOS SEUS DADOS PESSOAIS E O REGISTRO
            DE ATIVIDADES: Os seus dados pessoais serão eliminados pela equipe
            da Gafisa, quando expirar a sua finalidade e não forem mais
            necessários para cumprir qualquer obrigação legal. A exclusão de
            dados também pode acontecer quando o usuário solicitar o seu
            descarte, exceto se a manutenção do dado for expressamente
            autorizada por lei.
          </p>
          <p>
            As informações poderão ser preservadas para cumprimento de obrigação
            legal ou regulatória, transferência a terceiro sempre respeitando os
            requisitos de tratamento de dados.
          </p>
          <p>
            PRAZOS DE ARMAZENAMENTO EXCEDENTE: Para fins de auditoria,
            segurança, controle de fraudes, proteção ao crédito e preservação de
            direitos, poderemos permanecer com o histórico de registro de seus
            dados por prazo maior nas hipóteses que a lei ou norma regulatória
            assim estabelecer ou para preservação de direitos, inclusive as
            determinadas pela Associação Brasileira de Normas Técnicas (ABNT).
          </p>
          <p>
            Os Dados coletados serão armazenados em nossos servidores
            localizados no Brasil, bem como em ambiente de uso de recursos ou
            servidores na nuvem cloud computing.
          </p>
          <h2>DIREITOS DOS USUÁRIOS E COMO EXERCÊ-LOS:</h2>
          <p>
            DIREITOS BÁSICOS: você poderá solicitar ao nosso Encarregado de
            Dados Pessoais-DPO a confirmação da existência do tratamento de
            Dados Pessoais, além da exibição ou retificação de seus Dados
            Pessoais, por meio dos nossos Canais de Atendimento.
          </p>
          <p>
            LIMITAÇÃO, CONTRADIÇÃO E EXCLUSÃO DE DADOS: por meio dos nossos
            Canais de Atendimento, você poderá solicitar:
          </p>
          <ul>
            <li>A limitação do uso de seus Dados Pessoais;</li>
            <li>
              Declarar sua oposição e/ou revogar o consentimento quanto ao uso
              de seus Dados Pessoais;
            </li>
            <li>
              Requisitar a exclusão de seus Dados Pessoais que tenham sidos
              coletados por nós.
            </li>
          </ul>
          <p>
            Importante: a revogação é aplicada apenas quando a coleta for feita
            via consentimento. Caso você retire seu consentimento para
            finalidades fundamentais ao regular funcionamento das Plataformas e
            serviços, determinados ambientes e serviços poderão ficar
            indisponíveis para acesso. Se você solicitar a exclusão de seus
            dados pessoais, mas ainda exista alguma finalidade vigente para o
            dado, pode ocorrer que os dados precisem ser mantidos por período
            superior ao pedido de exclusão.
          </p>
          <h2 style={{ marginTop: '1rem' }}>Disposições gerais:</h2>
          <p>
            ALTERAÇÃO E ATUALIZAÇÃO DA POLÍTICA: A Companhia salienta que, é de
            nosso direito de alterar o teor desta Política a qualquer momento,
            conforme a finalidade ou necessidade, tal qual para adequação e
            conformidade legal de disposição de lei ou norma que tenha força
            jurídica equivalente, cabendo a você verificá-la sempre que efetuar
            o acesso à Plataforma ou utilizar nossos serviços. Ocorrendo
            atualizações neste documento e que demandem nova coleta de
            consentimento, você será notificado por meio dos canais de contatos
            que você informar.
          </p>
          <p>
            COMUNICAÇÃO ELETRÔNICA: Você reconhece que toda comunicação
            realizada por e-mail (aos endereços informados no seu cadastro),
            SMS, aplicativos de comunicação instantânea ou qualquer outra forma
            digital, também são válidas, eficazes e suficiente para a divulgação
            de qualquer assunto que se refira aos serviços que prestamos, aos
            seus Dados, bem como às condições de sua prestação ou a qualquer
            outro assunto nele abordado, sendo exceção apenas o que essa
            Política prever como tal.
          </p>
          <p>
            CANAIS DE ATENDIMENTO: Em caso de qualquer dúvida com relação às
            disposições constantes desta Política de Privacidade, você poderá
            entrar em contato por meio dos canais de atendimento apontados a
            seguir:
          </p>
          <ul>
            <li>Portal de Relacionamento para Clientes;</li>
            <li>Canal de Denúncia Anônima da Gafisa;</li>
            <li>
              E-mail do Encarregado da Proteção de Dados Pessoais / DPO:
              protecaodados@gafisa.com.br
            </li>
            <li>
              E-mail de Comunicação Corporativa:
              comunicacaocorporativa@gafisa.com.br
            </li>
          </ul>
          <p>
            APLICABILIDADE: Esta Política será interpretada segundo a legislação
            brasileira, sendo eleito o foro da Capital do Estado de São Paulo
            para dirimir qualquer controvérsia que envolva este documento, salvo
            ressalva específica de competência pessoal, territorial ou funcional
            pela legislação aplicável.
          </p>
          <p>
            PRAZO: Esta política tem validade a partir da data de sua
            publicação, podendo ser alterada a qualquer tempo e critério pelo
            Comitê Privacidade.
          </p>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Fechar</Button>
      </DialogActions>
    </Dialog>
  );
};
