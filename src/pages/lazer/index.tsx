import { useRouter } from "next/router";
import Head from "next/head";
import { useState } from "react";
import FsLightbox from "fslightbox-react";
import { Grid } from "@mui/material";

import Banner from "../../components/Banner";
import Header from "../../components/Header";
import Footer from "../../components/Footer";

import { Container } from "../../styles/global";
import {
  Section,
  ContainerTitle,
  ContainerMapa,
  Box,
  Option,
  ContainerSlide,
} from "../../styles/pages/lazer/style";
import { Button } from "../../components/Button";
import { SplideCarousel } from "../../components/Splide";

export default function Lazer() {
  const router = useRouter();
  const [lightboxControllerImages, setLightboxControllerImages] = useState({
    toggler: false,
    slide: 1,
  });

  const optionsLeft = [
    { number: 1, name: "Piscina infantil" },
    { number: 2, name: "Solário" },
    { number: 3, name: "Piscina adulto" },
    { number: 4, name: "Repouso" },
    { number: 5, name: "Deck molhado" },
    { number: 6, name: "Sauna" },
    { number: 7, name: "Academia" },
    { number: 8, name: "Academia" },
    { number: 9, name: "Festas / Coworking" },
  ];

  const optionsRight = [
    { number: 10, name: "Terraço festas" },
    { number: 11, name: "Lavabo social" },
    { number: 12, name: "Jogos" },
    { number: 13, name: "Terraço jogos" },
    { number: 14, name: "Brinquedoteca" },
    { number: 15, name: "Playground" },
    { number: 16, name: "Pet place" },
    {
      number: 17,
      name: "We Staff Room",
      description:
        "Área de apoio para seus funcionários. Assim, a plnta do seu apartamento ganha ainda mais flexibilidade, podendo ter um lavabo.",
    },
  ];

  const items = [
    {
      src: "/images/lazer/piscina.jpg",
      description: "Piscina",
      reference: "lazer",
    },
    {
      src: "/images/lazer/jogos-teen.jpg",
      description: "Jogos Teen",
      reference: "lazer",
    },
    {
      src: "/images/lazer/academia.jpg",
      description: "Academia",
      reference: "lazer",
    },
    {
      src: "/images/lazer/brinquedoteca.jpg",
      description: "Brinquedoteca",
      reference: "lazer",
    },
    {
      src: "/images/lazer/salao-de-festas.jpg",
      description: "Salão de Festas",
      reference: "lazer",
    },
    {
      src: "/images/lazer/pet-place.jpg",
      description: "Pet Place",
      reference: "lazer",
    },
    {
      src: "/images/lazer/playground.jpg",
      description: "Playground",
      reference: "lazer",
    },
  ];

  return (
    <>
      <Head>
        <title>Apartamento à venda em Botafogo | 3 e 4 quartos | GAFISA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Site da imobiliária performance" />
        <meta
          name="description"
          content="Receba mais informações sobre o lançamento da Gafisa no bairro de Botafogo, Rio de Janeiro. Clique e saiba mais."
        />
        <meta name="author" content="Internit" />
        <meta
          name="keywords"
          content="we sorocaba, gafisa, apartamento botafogo"
        />
      </Head>
      <Header showLogo={true} />
      <Banner page="Lazer" />
      <Section>
        <Container>
          <ContainerTitle>
            <h2>
              Espaço reservado <br />
              para momentos divertidos
            </h2>
            <span>Um Lazer mais que completo para 25 famílias</span>
          </ContainerTitle>
          <ContainerMapa>
            <Grid container spacing={3}>
              <Grid item xs={12} className="containerImage">
                <img
                  src="/images/lazer/planta.png"
                  width="auto"
                  alt="Planta de lazer"
                  onClick={() =>
                    setLightboxControllerImages({
                      toggler: !lightboxControllerImages.toggler,
                      slide: 1,
                    })
                  }
                />
              </Grid>
              <Grid item xs={12} className="containerText">
                <Box>
                  <Grid item xs={12}>
                    <Grid container spacing={2}>
                      <Grid item xs={6}>
                        {optionsLeft.map((option, index) => (
                          <Option key={`option-left-${index}`}>
                            <div className="number">
                              <span>{option.number}</span>
                            </div>
                            <div className="name">
                              <span>{option.name}</span>
                            </div>
                          </Option>
                        ))}
                      </Grid>
                      <Grid item xs={6}>
                        {optionsRight.map((option, index) => (
                          <Option key={`option-right-${index}`}>
                            <div className="number">
                              <span>{option.number}</span>
                            </div>
                            <div className="name">
                              <span>{option.name}</span>
                              {option?.description && (
                                <p>
                                  Área de apoio para seus funcionários. Assim, a
                                  planta do seu apartamento ganha ainda mais
                                  flexibilidade, podendo ter um lavabo.
                                </p>
                              )}
                            </div>
                          </Option>
                        ))}
                      </Grid>
                    </Grid>
                  </Grid>
                </Box>
              </Grid>
              <Grid item xs={12} marginTop={5}>
                <Button onClick={() => router.push("/contato")}>
                  solicite mais informações
                </Button>
              </Grid>
            </Grid>
          </ContainerMapa>
        </Container>
        <SplideCarousel
          nameSlide="lazer"
          items={items}
          description={true}
          magnifying={false}
        />
        <div className="button">
          <Button onClick={() => router.push("/contato")}>
            solicite mais informações
          </Button>
        </div>
        <FsLightbox
          toggler={lightboxControllerImages.toggler}
          sources={["/images/lazer/planta.png"]}
          slide={lightboxControllerImages.slide}
        />
      </Section>
      <Footer showLogos={true} information={false} />
    </>
  );
}
