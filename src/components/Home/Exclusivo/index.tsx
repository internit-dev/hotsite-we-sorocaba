import { useState } from "react";
import { Grid } from "@mui/material";
import { useRouter } from "next/router";
import { Button } from "../../Button";

import { Container } from "../../../styles/global";
import {
  Section,
  Header,
  ContainerImages,
  Address,
  ContainerText,
} from "./style";
import FsLightbox from "fslightbox-react";

export function Exclusivo() {
  const [lightboxControllerImages, setLightboxControllerImages] = useState({
    toggler: false,
    slide: 1,
  });
  const router = useRouter();

  return (
    <Section>
      <Container>
        <Header>
          <h1>We Sorocaba</h1>
          <p>Exclusivo para 25 famílias.</p>
        </Header>
        <ContainerImages>
          <Grid container>
            <Grid item xs={12} md={6} className="imageLeft">
              <div
                className="image"
                onClick={() =>
                  setLightboxControllerImages({
                    toggler: !lightboxControllerImages.toggler,
                    slide: 1,
                  })
                }
              >
                <img
                  src="/images/home/fachada.jpg"
                  alt="Fachada"
                  width="100%"
                />
                <div className="lupa">
                  <img src="/images/home/icons/lupa.png" alt="Lupa" />
                </div>
              </div>
              <p>Fachada</p>
            </Grid>
            <Grid item xs={12} md={6} className="imageRight">
              <div
                className="image"
                onClick={() =>
                  setLightboxControllerImages({
                    toggler: !lightboxControllerImages.toggler,
                    slide: 2,
                  })
                }
              >
                <img src="/images/home/rua.jpg" alt="Rua" width="100%" />
                <div className="lupa">
                  <img src="/images/home/icons/lupa.png" alt="Lupa" />
                </div>
              </div>

              <Address>
                <p>Rua Sorocaba, 701</p>
              </Address>
            </Grid>
          </Grid>
        </ContainerImages>
        <ContainerText>
          <p>
            Em uma das melhores ruas de Botafogo, um bairro que tem tudo para
            agradar a cada um de nós. De todas as idades. De todos os momentos.
            De todas as necessidades.
          </p>
          <Button onClick={() => router.push("/empreendimento")}>
            Saiba Mais
          </Button>
        </ContainerText>
      </Container>

      <FsLightbox
        toggler={lightboxControllerImages.toggler}
        sources={["/images/home/fachada.jpg", "/images/home/rua.jpg"]}
        slide={lightboxControllerImages.slide}
      />
    </Section>
  );
}
