import styled from "styled-components";

export const ContainerSlide = styled.div`
  margin: 5rem 0;

  @media (max-width: 767px){
    margin: 3rem 0;
  }
`;

export const Carousel = styled.div`
  .swiper {
    padding: 3rem 0;
    position: relative;

    .swiper-slide {
      transition: all 0.3s;
      cursor: pointer;
      position: relative;

      &.swiper-slide-next {
        @media (min-width: 1024px) {
          transform: scale(1.2);
        }
      }

      .magnifying {
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        background: rgba(0, 0, 0, 0);
        transition: all ease 0.3s;

        img {
          width: 10%;
          opacity: 0;
          transition: all ease 0.3s;
        }

        &:hover {
          background: rgba(0, 0, 0, 0.2);
          img {
            opacity: 1;
          }
        }
      }

      img {
        width: 100%;
      }

      span {
        color: ${({ theme }) => theme.white};
        text-transform: uppercase;
        font-size: 0.8rem;
        position: absolute;
        bottom: 1rem;
        left: 2rem;

        &::before {
          position: absolute;
          content: "[";
          left: -1rem;
          font-size: 2rem;
          top: -0.8rem;
          color: ${({ theme }) => theme.black};
          font-family: "Gotham Medium";
        }

        &::after {
          position: absolute;
          content: "]";
          right: -1rem;
          top: -0.8rem;
          font-size: 2rem;
          color: ${({ theme }) => theme.black};
          font-family: "Gotham Medium";
        }
      }
    }
    .prev,
    .next {
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      z-index: 99999;
      cursor: pointer;
      transition: all 0.3s;
      svg {
        width: 4rem;
        height: 4rem;

        @media (max-width: 1279px) {
          width: 3rem;
          height: 3rem;
        }
      }

      &.swiper-button-disabled {
        opacity: 0.3;
      }
    }

    .prev {
      left: 35rem;

      @media (min-width: 2159px) {
        left: 47rem;
      }

      @media (max-width: 1680px) {
        left: 24.5rem;
      }

      @media (max-width: 1279px) {
        left: 20rem;
      }

      @media (max-width: 1023px) {
        left: 1rem;
      }
    }
    .next {
      right: 35rem;

      @media (min-width: 2159px) {
        right: 47rem;
      }

      @media (max-width: 1680px) {
        right: 24.5rem;
      }

      @media (max-width: 1279px) {
        right: 20rem;
      }

      @media (max-width: 1023px) {
        right: 1rem;
      }
    }
  }
`;
