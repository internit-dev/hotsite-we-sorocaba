import { Grid } from "@mui/material";

import Banner from "../../components/Banner";
import Header from "../../components/Header";
import Footer from "../../components/Footer";

import { Container } from "../../styles/global";
import {
  Section,
  ContainerTitle,
  ContainerInformation,
  ContainerText,
} from "../../styles/pages/viverbem/style";
import Head from "next/head";

export default function ViverBem() {
  return (
    <>
      <Head>
        <title>Apartamento à venda em Botafogo | 3 e 4 quartos | GAFISA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Site da imobiliária performance" />
        <meta
          name="description"
          content="Receba mais informações sobre o lançamento da Gafisa no bairro de Botafogo, Rio de Janeiro. Clique e saiba mais."
        />
        <meta name="author" content="Internit" />
        <meta
          name="keywords"
          content="we sorocaba, gafisa, apartamento botafogo"
        />
      </Head>
      <Header showLogo={true} />
      <Banner page="Viver Bem" />
      <Section>
        <Container>
          <ContainerTitle>
            <img
              src="/images/logo-viver-bem.png"
              width="50%"
              alt="Gafisa Viver Bem"
            />
            <h3>Conceitos e projetos inovadores</h3>
            <h2>nos melhores espaços.</h2>
          </ContainerTitle>
          <ContainerInformation>
            <Grid container spacing={2}>
              <Grid item md={6} xs={12} className="firstContainerLeft">
                <ContainerText>
                  <h3>viver bem.</h3>
                  <p>O nosso Programa de Relacionamento com o Cliente.</p>
                  <p>
                    Como tudo que a Gafisa faz é pensando no bem-estar dos
                    clientes, criamos um Programa de Relacionamento no setor: o
                    Viver Bem.
                  </p>
                  <p>
                    O programa tem o objetivo de levar ao cliente Gafisa, de
                    maneira simples e transparente, informações importantes e
                    pertinentes sobre cada fase da construção de seu
                    empreendimento, desde o lançamento até depois da entrega do
                    seu imóvel.
                  </p>
                  <p>
                    O programa oferece também diversas vantagens e benefícios
                    exclusivos.
                  </p>
                </ContainerText>
              </Grid>
              <Grid item md={6} xs={12} className="firstContainerRight">
                <img
                  src="/images/viverBem/image-1.svg"
                  width="100%"
                  alt="Pessoas no computador"
                />
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
                className="secondContainerLeft"
                marginTop={5}
              >
                <img
                  src="/images/viverBem/image-2.svg"
                  width="100%"
                  alt="Pessos arrumando casa"
                />
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
                className="secondContainerRight"
                marginTop={5}
              >
                <ContainerText>
                  <p>
                    Viver Bem sempre foi uma inspiração para a Gafisa e isso nos
                    levou a desenvolver os melhores espaços e idealizar
                    conceitos e projetos inovadores.
                  </p>
                  <p>
                    Mas viver Bem é mais do que isso, é o nosso propósito. É o
                    que dá sentido a tudo aquilo que a gente faz para ter sempre
                    ao nosso lado nosso maior bem, você.
                  </p>
                  <p>
                    Seja bem-vindo ao Viver Bem Gafisa, um programa de
                    relacionamento completo para você e para o seu Gafisa.
                  </p>
                  <p>
                    No Viver Bem, você pode personalizar antes mesmo de pegar as
                    chaves, decorar ou reformar sua unidade com descontos
                    especiais. Você tem acesso também a um clube de compras
                    exclusivo com produtos de grandes fabricantes de
                    eletrodomésticos e decoração direto da fábrica e sem
                    intermediários.
                  </p>
                  <p>
                    O Viver Bem Gafisa vai acompanha-lo em todos os momentos
                    desde a aquisição do seu Gafisa, até mesmo quando você já
                    estiver morando nele, tudo para que a sua experiência seja
                    única.
                  </p>
                  <p>
                    Facilitar a vida de quem acaba de adquirir um imóvel e
                    executar tudo com a máxima atenção e qualidade foram as
                    premissas que impulsionaram a criação do Viver Bem Gafisa.
                  </p>
                  <p>
                    A área de personalização da Gafisa conta com a mesma
                    expertise de quem desenvolve produtos únicos e inovadores
                    com foco em trazer soluções práticas e personalizadas para
                    você.
                  </p>
                  <h3>viva bem com a Gafisa.</h3>
                </ContainerText>
              </Grid>
            </Grid>
          </ContainerInformation>
        </Container>
      </Section>
      <Footer showLogos={false} information={false} />
    </>
  );
}
