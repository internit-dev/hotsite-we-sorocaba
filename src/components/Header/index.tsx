import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useSideBar } from "../../hooks/sidebar";
import { ActiveLink } from "../ActiveLink";

import {
  Header,
  Container,
  ContainerLogo,
  ContainerOptions,
  Option,
  SideBar,
  ButtonOpenMenu,
} from "./style";

interface HeaderAplicationLogo {
  showLogo: boolean;
}

export default function HeaderAplication({ showLogo }: HeaderAplicationLogo) {
  const [scroll, setScroll] = useState(false);
  const [widthScreen, setWidthScreen] = useState(0);
  const { isSideBarOpen, handleSideBar } = useSideBar();
  const [utmKeeper, setUtmKeeper] = useState("");
  const router = useRouter();

  useEffect(() => {
    onScroll();
  });

  useEffect(() => {
    setUTMAnalytics();
  }, []);

  function onScroll() {
    window.addEventListener("scroll", function () {
      const scroll = this.scrollY;
      if (scroll > 100) {
        setScroll(true);
      } else {
        setScroll(false);
      }
    });
    setWidthScreen(window.innerWidth);
  }

  function handleClickOpen() {
    setTimeout(() => {
      handleSideBar(!isSideBarOpen);
    }, 1000);
  }

  function setUTMAnalytics() {
    const url = window.location.search;
    if (url) {
      setUtmKeeper(url);
    }
  }

  return (
    <Header
      className={scroll || widthScreen <= 1023 ? "scrolled" : ""}
      showLogo={showLogo}
      widthScreen={widthScreen}
    >
      <Container>
        <ContainerLogo>
          <img
            src="/images/header/logo.png"
            width="auto"
            alt="Logo We Sorocaba"
            onClick={() => router.push("/")}
          />
        </ContainerLogo>

        <ButtonOpenMenu onClick={() => handleSideBar(!isSideBarOpen)}>
          <img
            src="/images/header/menu-aberto.png"
            width="auto"
            alt="Ícone para abrir menu"
          />
        </ButtonOpenMenu>
      </Container>

      <ContainerOptions>
        <Option>
          <ActiveLink
            scroll={scroll}
            activeClassName="active"
            href={`/empreendimento${utmKeeper && utmKeeper}`}
            prefetch
          >
            <a>Empreendimento</a>
          </ActiveLink>
        </Option>
        <Option>
          <ActiveLink
            scroll={scroll}
            activeClassName="active"
            href={`/flex4you${utmKeeper && utmKeeper}`}
            prefetch
          >
            <a>Flex 4 you</a>
          </ActiveLink>
        </Option>
        <Option>
          <ActiveLink
            scroll={scroll}
            activeClassName="active"
            href={`/lazer${utmKeeper && utmKeeper}`}
            prefetch
          >
            <a>Lazer</a>
          </ActiveLink>
        </Option>
        <Option>
          <ActiveLink
            scroll={scroll}
            activeClassName="active"
            href={`/localizacao${utmKeeper && utmKeeper}`}
            prefetch
          >
            <a>Localização</a>
          </ActiveLink>
        </Option>
        <Option>
          <ActiveLink
            scroll={scroll}
            activeClassName="active"
            href={`/realizacao${utmKeeper && utmKeeper}`}
            prefetch
          >
            <a>Realização</a>
          </ActiveLink>
        </Option>
        <Option>
          <ActiveLink
            scroll={scroll}
            activeClassName="active"
            href={`/viverbem${utmKeeper && utmKeeper}`}
            prefetch
          >
            <a>Viver bem</a>
          </ActiveLink>
        </Option>
        <Option className="contato">
          <ActiveLink
            scroll={scroll}
            activeClassName="active"
            href={`/contato${utmKeeper && utmKeeper}`}
            prefetch
          >
            <a>Contato</a>
          </ActiveLink>
        </Option>
      </ContainerOptions>

      {/* Menu mobile */}
      <SideBar menuOpen={isSideBarOpen}>
        <Option onClick={handleClickOpen}>
          <ActiveLink
            scroll={scroll}
            activeClassName="active"
            href={`/empreendimento${utmKeeper && utmKeeper}`}
            prefetch
          >
            <a>Empreendimento</a>
          </ActiveLink>
        </Option>
        <Option onClick={handleClickOpen}>
          <ActiveLink
            scroll={scroll}
            activeClassName="active"
            href={`/flex4you${utmKeeper && utmKeeper}`}
            prefetch
          >
            <a>Flex 4 you</a>
          </ActiveLink>
        </Option>
        <Option onClick={handleClickOpen}>
          <ActiveLink
            scroll={scroll}
            activeClassName="active"
            href={`/lazer${utmKeeper && utmKeeper}`}
            prefetch
          >
            <a>Lazer</a>
          </ActiveLink>
        </Option>
        <Option onClick={handleClickOpen}>
          <ActiveLink
            scroll={scroll}
            activeClassName="active"
            href={`/localizacao${utmKeeper && utmKeeper}`}
            prefetch
          >
            <a>Localização</a>
          </ActiveLink>
        </Option>
        <Option onClick={handleClickOpen}>
          <ActiveLink
            scroll={scroll}
            activeClassName="active"
            href={`/realizacao${utmKeeper && utmKeeper}`}
            prefetch
          >
            <a>Realização</a>
          </ActiveLink>
        </Option>
        <Option onClick={handleClickOpen}>
          <ActiveLink
            scroll={scroll}
            activeClassName="active"
            href={`/viverbem${utmKeeper && utmKeeper}`}
            prefetch
          >
            <a>Viver bem</a>
          </ActiveLink>
        </Option>
        <Option onClick={handleClickOpen} className="contato">
          <ActiveLink
            scroll={scroll}
            activeClassName="active"
            href={`/contato${utmKeeper && utmKeeper}`}
            prefetch
          >
            <a>Contato</a>
          </ActiveLink>
        </Option>
        {/* <Option>
          <Link href="">
            <a>
              {scroll || widthScreen <= 1023 ? (
                <img
                  src="/images/header/dark-icon-message.png"
                  width="auto"
                  alt="Ícone de mensagem"
                />
              ) : (
                <img
                  src="/images/header/icon-message.png"
                  width="auto"
                  alt="Ícone de mensagem"
                />
              )}
            </a>
          </Link>
        </Option> */}
      </SideBar>
    </Header>
  );
}
