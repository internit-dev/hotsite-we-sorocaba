import axios from "axios";

/**
 * Integraçã́o com CRM Hypnobox
 *
 * Mandar Post para {https://gafisa.hypnobox.com.br/email.receber.php} com parâmetros abaixo:
 *
 * @param idProduto* = Codigo do produto dentro do crm
 * @param nome* = Nome
 * @param email* = Email
 * @param ddd* = DDD
 * @param telefone* = Telefone Sem DDD
 * @param mensagem = Mensagem
 * @param midia = tipo do lead | Padrão {organico}
 * @param utm_source = parâmetros da url {utm_source}
 * @param utm_medium = parâmetros da url {utm_medium}
 * @param utm_campaign = parâmetros da url {utm_campaign}
 * @returns {*}
 */
async function sendToHypnobox(
  idProduto: string,
  nome: string,
  email: string,
  ddd: string,
  telefone: string,
  mensagem = "",
  midia = "organico",
  utm_source = "",
  utm_medium = "",
  utm_campaign = ""
) {
  const formData = new FormData();
  formData.append("id_produto", idProduto);
  formData.append("nome", nome);
  formData.append("email", email);
  formData.append("ddd_celular", ddd);
  formData.append("tel_celular", telefone);
  formData.append("mensagem", mensagem);
  formData.append("midia", midia);
  formData.append("utm_source", utm_source);
  formData.append("utm_medium", utm_medium);
  formData.append("utm_campaign", utm_campaign);
  return await axios.post(
    "https://gafisa.hypnobox.com.br/email.receber.php",
    formData,
    {
      headers: {
        "Content-Type": "text/html; charset=utf-8",
      },
      timeout: 8000,
    }
  );
}

export default sendToHypnobox;
