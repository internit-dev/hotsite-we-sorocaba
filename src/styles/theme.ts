const theme = {
    grena: "#8F392A",
    black: "#1F1A16",
    white: "#FFFFFF",
    ice: "#FEF9F3",
    green: "#A69766"
};

export default theme;