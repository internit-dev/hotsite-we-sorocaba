import styled from "styled-components";

export const Section = styled.section`
  text-align: center;

  .swiper {
    padding: 3rem 0;
    .swiper-slide {
      border: solid 1px ${({ theme }) => theme.grena};
      img {
        width: 100%;
      }
    }
  }
`;

export const ContainerPlantas = styled.div`
  margin-bottom: 2rem;
`;

export const Title = styled.div`
  h2 {
    font-family: "Gotham Bold";
    font-size: 3.3rem;
    text-align: center;
    padding-right: 15px;
    padding-left: 15px;
    margin-bottom: 1.5rem;

    @media (max-width: 767px){
      font-size: 2.5rem;
    }
  }

  p{
    max-width: 1280px;
    margin: 0 auto;
    line-height: 25px;

    @media (max-width: 767px){
      padding: 0 .5rem;
    }
  }
`;
