import styled from "styled-components";

interface HeaderProps {
  showLogo: boolean;
  widthScreen: number;
}

interface SidebarProps {
  menuOpen: boolean;
}

export const Header = styled.header<HeaderProps>`
  position: fixed;
  background-color: ${({ theme }) => theme.ice};
  transition: background-color 0.3s ease-in-out;
  z-index: 1000;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 1rem;
  flex-direction: ${(props) => (props.widthScreen <= 1023 ? "column" : "row")};
`;

export const Container = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  padding: 0 1rem;
  flex: 1;
`;

export const ContainerLogo = styled.div`
  cursor: pointer;
  img {
    width: 7rem;
  }
`;

export const ContainerOptions = styled.ul`
  display: flex;
  list-style: none;
  align-items: center;
  padding: 0 1rem;

  @media (max-width: 1279px) {
    display: none;
  }
`;

export const Option = styled.li`
  text-transform: uppercase;
  a {
    color: ${({ theme }) => theme.black};
    text-decoration: none;
    transition: color 0.3s ease-in-out;
    font-family: "Gotham Light";

    &.active {
      font-family: "Gotham Medium";
    }
  }

  & + li {
    margin-left: 2rem;
  }

  &.contato {
    position: relative;

    &::before {
      background: url(/images/home/colcheteLeft.png);
      width: 7px;
      left: -0.5rem;

      @media (max-width: 1280px) {
        left: 21rem;
      }

      @media (max-width: 767px) {
        left: 9rem;
      }
    }

    &::after {
      background: url(/images/home/colcheteRight.png);
      width: 8px;
      right: -0.5rem;

      @media (max-width: 1280px) {
        right: 21rem;
      }

      @media (max-width: 767px) {
        right: 9rem;
      }
    }

    &::after,
    &::before {
      position: absolute;
      content: "";
      background-size: contain;
      height: 41px;
      top: -0.7rem;
    }
  }
`;

export const SideBar = styled.div<SidebarProps>`
  width: 100%;
  position: absolute;
  top: 0;
  list-style: none;
  background: ${({ theme }) => theme.ice};
  text-align: center;
  transition: all 0.7s linear;
  transform: ${(props) =>
    props.menuOpen ? "translateY(6rem)" : "translateY(-21rem)"};
  padding-bottom: 1rem;

  @media (min-width: 1280px) {
    display: none;
  }

  li {
    a {
      color: ${({ theme }) => theme.black} !important;
    }
    & + li {
      margin-top: 1rem;
    }
  }
`;

export const ButtonOpenMenu = styled.button`
  background: transparent;
  border: none;

  @media (min-width: 1280px) {
    display: none;
  }
`;
