import styled from "styled-components";

export const Section = styled.section`
  margin: 4rem 0;
`;

export const ContainerImage = styled.div`
  position: relative;

  span {
    position: absolute;
    bottom: 2rem;
    left: 50%;
    transform: translateX(-50%);
    text-transform: uppercase;
    font-family: "Gotham Medium";
    font-size: 1.2rem;
    &::before {
      position: absolute;
      content: "[";
      color: ${({ theme }) => theme.grena};
      font-size: 3rem;
      left: -1.5rem;
      top: -1.1rem;

      @media (max-width: 1023px) {
        top: -1.3rem;
      }
    }

    &::after {
      position: absolute;
      content: "]";
      color: ${({ theme }) => theme.grena};
      font-size: 3rem;
      right: -1.5rem;
      top: -1.1rem;

      @media (max-width: 1023px) {
        top: -1.3rem;
      }
    }

    @media (max-width: 1919px) {
      font-size: 1rem;
    }

    @media (max-width: 1280px) {
      width: 70%;
    }

    @media (max-width: 1023px) {
      width: 78%;
      bottom: 1.2rem;
    }

    @media (max-width: 767px) {
      width: 90%;
      bottom: -3rem;
      text-align: center;
    }
  }
`;

export const ContainerGafisa = styled.div`
  margin-top: 6rem;
  .containerLeft {
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;

    &::before {
      position: absolute;
      content: "";
      background: url(/images/realizacao/colcheteRed.png);
      width: 96px;
      height: 538px;
      left: 0;

      @media (max-width: 1280px) {
        display: none;
      }

      @media (max-width: 1023px) {
        display: block;
        height: 463px;
        width: 83px;
        background-size: contain;
        left: 1rem;
      }

      @media (max-width: 767px) {
        display: none;
      }
    }

    @media (max-width: 1023px) {
      margin-bottom: 4rem;
    }

    @media (max-width: 767px) {
      margin-bottom: 2rem;
    }
  }

  .containerVisite {
    text-align: center;
    a {
      text-decoration: none;
      color: ${({ theme }) => theme.black};
      > div {
        width: 25%;
        margin: 0 auto;
        position: relative;
        h5 {
          font-size: 2.5rem;
          margin-bottom: 0.8rem;
        }

        p {
          font-family: "Gotham Light";
          font-weight: 600;
          font-size: 1.3rem;
          text-transform: uppercase;
        }

        &::before {
          position: absolute;
          content: "";
          background: url(/images/realizacao/colcheteBlackLeft.png);
          width: 18px;
          height: 100px;
          left: 0;
          top: -1rem;
        }

        &::after {
          position: absolute;
          content: "";
          background: url(/images/realizacao/colcheteBlackRight.png);
          width: 19px;
          height: 100px;
          right: 0;
          top: -1rem;
        }

        @media (max-width: 1919px) {
          width: 28%;
        }

        @media (max-width: 1280px) {
          width: 38%;
        }

        @media (max-width: 767px) {
          width: 92%;
        }
      }
    }
  }
`;

export const Text = styled.div`
  width: 75%;
  margin: 0 auto;
  h2 {
    font-size: 2.5rem;
    font-family: "Gotham Bold";
    margin-bottom: 2rem;
  }

  p {
    font-family: "Gotham Light";
    font-weight: 600;
    font-size: 0.9rem;
    & + p {
      margin-top: 1.5rem;
    }
  }

  @media (max-width: 1023px) {
    width: 80%;
  }

  @media (max-width: 767px) {
    width: 100%;
  }
`;
