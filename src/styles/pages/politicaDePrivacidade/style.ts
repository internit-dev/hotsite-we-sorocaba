import styled from "styled-components";

export const Section = styled.section`
  margin: 5rem 0;
`;

export const ContainerTitle = styled.div`
  text-align: center;
  margin-bottom: 5rem;

  h2 {
    font-size: 3rem;
  }
`;

export const ContainerInformation = styled.div`
  font-weight: 400;
  font-size: 1rem;
  line-height: 1.5;
  letter-spacing: 0.00938em;
  color: rgba(0, 0, 0, 0.6);


  h2, h3{
    color: #1F1A16;
  }

  p{
    font-family: "Roboto","Helvetica","Arial",sans-serif;
  }

  li{
    list-style-position: inside;
  }
`