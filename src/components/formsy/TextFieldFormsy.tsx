import React from "react";
import { withFormsy } from "formsy-react";
import { TextField } from "@mui/material";
import { styled } from "@mui/material/styles";
import _ from "../../@lodash";

const TextFieldCustomized = styled(TextField)({
	"& .MuiInput-root:before": {
		borderColor: "#8F392A",
	},
	"& .MuiInput-root:after": {
		borderColor: "#8F392A",
	},
	"& .MuiInput-root:hover:not(.Mui-disabled):before": {
		borderColor: "#8F392A",
	},
	"& label": {
		color: "#050706",
		fontFamily: 'Work Sans, sans-serif',
	},
	"& .Mui-focused": {
		color: "#8F392A !important",
	}
});

function TextFieldFormsy(props: any) {
	const importedProps = _.pick(props, [
		"autoComplete",
		"autoFocus",
		"children",
		"className",
		"defaultValue",
		"disabled",
		"FormHelperTextProps",
		"fullWidth",
		"id",
		"InputLabelProps",
		"inputProps",
		"InputProps",
		"inputRef",
		"label",
		"multiline",
		"name",
		"onBlur",
		"onChange",
		"onFocus",
		"placeholder",
		"required",
		"rows",
		"rowsMax",
		"select",
		"SelectProps",
		"type",
		"variant",
		"color",
	]);

	const { errorMessage } = props;
	const value = props.value || "";

	function changeValue(event: any) {
		props.setValue(event.currentTarget.value);
		if (props.onChange) {
			props.onChange(event);
		}
		if (props.onBlur) {
			props.onBlur(event);
		}
	}
	return (
		<TextFieldCustomized
			{...importedProps}
			onChange={changeValue}
			value={value}
			error={Boolean(
				(!props.isPristine && props.showRequired) || errorMessage
			)}
			helperText={errorMessage}
			style={{ width: "100%" }}
		/>
	);
}

export default React.memo(withFormsy(TextFieldFormsy));
