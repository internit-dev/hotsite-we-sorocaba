import axios from "axios";

/**
 * Integraçã́o com Novo CRM
 *
 * @param idCrm
 * @param nome
 * @param email
 * @param ddd
 * @param telefone
 * @param mensagem
 * @param utm_source
 * @param utm_medium
 * @param utm_campaign
 */
async function sendToCRM(  idCrm: string,
                           nome: string,
                           email: string,
                           ddd: string,
                           telefone: string,
                           mensagem = "",
                           utm_source = "",
                           utm_medium = "",
                           utm_campaign = "") {


    let formData = new FormData();
    formData.append("apiNome", nome);
    formData.append("apiEmail", email);
    formData.append("apiTelefone1", ddd+telefone);
    formData.append("apiMensagem", mensagem);
    formData.append("apiDispositivo", getDevice());
    formData.append("apiOrigem", 'site');
    formData.append("utm_source", utm_source);
    formData.append("utm_medium", utm_medium);
    formData.append("utm_campaign", utm_campaign);


    return await axios.post('https://www.novocrm.atendimentoon.com.br/api/form/externo/' + idCrm,
        formData,
        {
            timeout: 5000,
        }
    );
}

/**
 * Retorna Dispositivo - Mobile | Desktop
 */
function getDevice(){
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        return 'Mobile'
    }
    return 'Desktop'
}


export default sendToCRM;
