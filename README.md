# Hotsite We Sorocaba
# Desenvolvedor: Grazielle Conceição

Este site foi desenvolvido com NextJS.

## Iniciando projeto

Rode o comando abaixo para instalar as dependências do projeto.

```bash yarn```
Em seguida execute ``yarn dev`` para rodar o projeto e acesse em [http://localhost:3000](http://localhost:3000) no navegador.

## Estrutura do projeto

As imagens do projeto encontram-se na pasta **PUBLIC/IMAGES**.

Todos os arquivos de HTML/CSS de suas respectivas páginas estarão dentro da pasta **SRC/PAGES**.

As seções da home encontram-se dentro da pasta **COMPONENTS/HOME**, assim como os demais componentes globais da aplicação.