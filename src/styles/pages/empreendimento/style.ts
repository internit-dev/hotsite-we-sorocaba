import styled from "styled-components";

export const Section = styled.section`
  margin: 6rem 0;

  > div {
      text-align: center;
      button{
          margin-top: 5rem;
          color: #6A072F;
      }
  }
`;

export const ContainerHeader = styled.div`
  text-align: center;

  .title {
    h2 {
      font-family: "Gotham Medium";
      font-size: 3.6rem;

      @media (max-width: 767px) {
        font-size: 2.6rem;
      }
    }

    h6 {
      font-size: 1.8rem;
      font-family: "Gotham Light";
      margin-top: 0.8rem;
      

      @media (max-width: 767px) {
        font-size: 1.3rem;
      }
    }
  }

  .subtitle {
    margin-top: 3.1rem;

    p {
      font-size: 1.2rem;
      max-width: 67%;
      margin: 0 auto;

      @media (max-width: 1023px) {
        max-width: 100%;
      }
    }
  }
`;

export const ContainerImages = styled.div`
  margin-top: 6rem;
  position: relative;

  &::before,
  &::after {
    position: absolute;
    content: "";
    background-size: contain !important;
    z-index: 999;
  }

  &::before {
    background: url(/images/brown-bracket-left2.png);
    left: 1rem;
    top: 14rem;
    width: 4.9rem;
    height: 26.7rem;

    @media (max-width: 1280px) {
      width: 3.6rem;
      height: 20rem;
    }

    @media (max-width: 1023px) {
      width: 2.9rem;
      height: 16rem;
      top: 9rem;
    }
  }

  &::after {
    background: url(/images/brown-bracket-right2.png);
    right: 1rem;
    top: -3.5rem;
    width: 4.55rem;
    height: 25rem;

    @media (max-width: 1280px) {
      width: 3.4rem;
      height: 18.5rem;
    }

    @media (max-width: 1023px) {
      width: 3rem;
      height: 16.5rem;
      top: -2.5rem;
    }
  }

  .containerLeft {
    .first-image {
      span {
        right: 2rem;
      }
    }

    .second-image {
      padding-right: 2.5rem;
      margin-top: 1.5rem;

      span {
        left: 2rem;
      }

      @media (max-width: 767px) {
        padding-right: 0;
      }
    }

    div {
      position: relative;

      span {
        position: absolute;
        bottom: 1.5rem;
        color: ${({ theme }) => theme.white};
        text-transform: uppercase;
        font-size: 0.8rem;
        font-weight: 500;

        &::before {
          position: absolute;
          content: "[";
          color: ${({ theme }) => theme.black};
          font-size: 2.3rem;
          font-weight: bolder;
          font-family: "Gotham Bold";
          top: -1rem;
          left: -1rem;
        }

        &::after {
          position: absolute;
          content: "]";
          color: ${({ theme }) => theme.black};
          font-size: 2.3rem;
          font-weight: bolder;
          font-family: "Gotham Bold";
          top: -1rem;
          right: -1rem;
        }
      }
    }
  }

  .containerRight {
    .first-image {
      padding-left: 2.5rem;
      margin-bottom: 1.5rem;
      position: relative;

      span {
        position: absolute;
        bottom: 1.5rem;
        right: 2rem;
        font-size: 0.8rem;
        font-weight: 500;
        text-transform: uppercase;
        color: ${({ theme }) => theme.white};

        &::before {
          position: absolute;
          content: "[";
          font-size: 2.3rem;
          font-weight: bolder;
          font-family: "Gotham Bold";
          top: -1rem;
          left: -1rem;
          color: ${({ theme }) => theme.black};
        }

        &::after {
          position: absolute;
          content: "]";
          font-size: 2.3rem;
          font-weight: bolder;
          font-family: "Gotham Bold";
          top: -1rem;
          right: -1rem;
          color: ${({ theme }) => theme.black};
        }
      }

      @media (max-width: 767px) {
        padding-left: 0;
        margin-top: 1.5rem;
      }
    }
  }
`;

export const ContainerEscritorios = styled.div`
  margin-top: 3rem;
  > div {
    text-align: left;

    .subtitle {
      margin-top: 2.1rem;
      p {
        max-width: 54%;
        margin-left: 0;

        @media (max-width: 1280px){
            max-width: 100%;
        }
      }
    }
  }

  .container {
    position: relative;

    &::before,
    &::after {
      position: absolute;
      content: "";
      background-size: contain !important;
      z-index: 999;
    }

    &::before {
      background: url(/images/brown-bracket-left2.png);
      left: -2rem;
      top: 26rem;
      width: 4.4rem;
      height: 24rem;

      @media (min-width: 1281px) {
        left: 1rem;
        top: 29rem;
      }

      @media (max-width: 1280px) {
        width: 3.6rem;
        height: 20rem;
        left: 1rem;
        top: 25rem;
      }

      @media (max-width: 1023px) {
        width: 2.7rem;
        height: 15rem;
        top: 19rem;
      }

      @media (max-width: 767px) {
        display: none;
      }
    }

    &::after {
      background: url(/images/brown-bracket-right2.png);
      right: -2.8rem;
      top: -2.5rem;
      width: 4.5rem;
      height: 24.6rem;

      @media (min-width: 1281px) {
        right: 0;
        top: -1.5rem;
      }

      @media (max-width: 1280px) {
        width: 3.5rem;
        height: 19rem;
        right: 0;
      }

      @media (max-width: 1023px) {
        width: 2.55rem;
        height: 14rem;
        top: -0.5rem;
        right: 0
      }

      @media (max-width: 767px) {
        display: none;
      }
    }
  }
`;

export const Project = styled.div`
  padding-left: 10rem;

  &.first {
    padding-left: 0;
    margin-bottom: 4rem;

    @media (max-width: 767px) {
      margin-top: 0;
    }

    .description,
    .logo {
      padding-left: 4rem;
    }
  }

  &:last-child {
    padding-left: 6rem;
    margin-top: 4rem;

    @media (max-width: 1280px) {
      padding-left: 2rem;
    }

    @media (max-width: 767px) {
      padding-left: 0;
      margin-top: 0;
    }
  }

  .logo {
    margin-top: 2rem;

    img {
      width: auto;

      @media (max-width: 1280px) {
        max-width: 100%;
      }
    }
  }

  .description {
    margin-top: 0.8rem;

    span {
      font-family: "Gotham Medium";
      font-size: 0.8rem;
      text-transform: uppercase;
      color: ${({ theme }) => theme.grena};
    }

    p {
      font-size: 0.9rem;
      line-height: 14px;
      margin-top: 0.8rem;
    }
  }

  &.last {
    position: relative;

    &::after {
      position: absolute;
      content: "";
      background: url(/images/brown-bracket-right2.png);
      right: -6.8rem;
      top: 9.5rem;
      width: 5.55rem;
      height: 25rem;
      background-size: contain;

      @media (min-width: 1281px) {
        width: 4.55rem;
        right: 0;
      }

      @media (max-width: 1280px) {
        width: 4.4rem;
        right: 0;
        top: 3.5rem;
        height: 24rem;
      }

      @media (max-width: 1023px) {
        width: 3.55rem;
        height: 19.3rem;
        top: 5rem;
        right: 0;
      }

      @media (max-width: 767px) {
        display: none;
      }
    }
  }

  @media (max-width: 1280px) {
    padding-left: 4rem;
  }

  @media (max-width: 767px) {
    padding-left: 5rem;
  }
`;
