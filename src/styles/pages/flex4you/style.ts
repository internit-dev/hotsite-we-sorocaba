import styled from "styled-components";

export const Section = styled.section`
  margin: 5rem 0;

  .swiper-slide {
    border: 1px solid #1f1a16;
  }

  > button {
    margin: 1rem 0;
  }

  > div {
    &.button {
      width: 100%;
      text-align: center;
    }
  }
`;

export const ContainerHeader = styled.div`
  text-align: center;
  .title {
    h4 {
      margin-bottom: 0;
      font-size: 1.8rem;
      @media (max-width: 767px) {
        font-size: 1.5rem;
      }
    }

    h2 {
      font-family: "Gotham Medium";
      font-size: 3.6rem;

      @media (max-width: 767px) {
        font-size: 2.8rem;
      }
    }
  }

  .description {
    max-width: 72%;
    margin: 2rem auto 4rem auto;

    p {
      font-family: "Gotham Light";
      font-weight: 600;
      font-size: 1.2rem;
      line-height: 25px;

      @media (max-width: 1919px) {
        font-size: .9rem;
      }

      @media (max-width: 767px) {
        font-size: 1rem;
      }
    }

    @media (max-width: 767px) {
      max-width: 100%;
    }
  }
`;

export const ContainerInformation = styled.div`
  .containerImage {
    img {
      width: 100%;
    }
  }
`;

export const Box = styled.div`
  padding-top: 2rem;
  padding-left: 3rem;

  @media (max-width: 767px) {
    padding-left: 0;
  }
`;

export const Logo = styled.div`
  margin-bottom: 2rem;

  img {
    width: auto;
  }
`;

export const Valores = styled.div`
  span {
    text-transform: uppercase;

    & + h5 {
      margin-bottom: 2rem;
    }
  }

  h5 {
    color: ${({ theme }) => theme.grena};
    font-size: 2.5rem;
    font-family: "Gotham Bold";
    margin-bottom: 1rem;
  }
`;

export const FooterBox = styled.div`
  position: relative;
  padding-top: 2rem;
  margin-top: 2rem;

  &::before {
    position: absolute;
    content: "";
    width: 150px;
    height: 2px;
    background-color: #000;
    top: 0;
  }

  span {
    color: ${({ theme }) => theme.grena};
    text-transform: uppercase;
    font-size: 1.3rem;
  }
`;

export const ContainerFlex4You = styled.div`
  margin-top: 8rem;
  margin-bottom: 6rem;

  > div {
    background-color: ${({ theme }) => theme.grena};
    height: 38.1rem;
    position: relative;
    > div {
      position: relative;

      &::before {
        position: absolute;
        content: "";
        background: url(/images/flex4you/bloco.svg);
        width: 79px;
        height: 440px;
        top: 3rem;
        left: -6rem;

        @media (max-width: 1919px) {
          left: -3rem;
        }

        @media (max-width: 1199px) {
          left: 1rem;
        }

        @media (max-width: 767px) {
          height: 359px;
          width: 63px;
          background-size: contain;
          top: 5rem;
          left: .5rem;
        }
      }
    }

    @media (max-width: 1199px) {
      height: auto;
      padding-left: 0;
    }
  }

  .containerText {
    .logo {
      text-align: right;
      transform: translate(-1rem, -4rem);

      img {
        width: 25%;

        @media (max-width: 1199px) {
          width: 32%;
        }

        @media (max-width: 767px) {
          width: 40%;
        }
      }
    }

    .description {
      h3 {
        font-family: "Bodoni MT";
        color: ${({ theme }) => theme.white};
        font-size: 3rem;
        font-weight: 400;
        margin-bottom: 1rem;
      }

      p {
        text-transform: uppercase;
        color: ${({ theme }) => theme.white};
        font-size: 1.5rem;
        width: 68%;
        line-height: 33px;
        margin-bottom: 1rem;

        @media (max-width: 1199px) {
          width: 100%;
          font-size: 1.3rem;
          line-height: 25px;
        }
      }

      span {
        color: ${({ theme }) => theme.white};
        font-size: 1rem;
      }
    }

    @media (max-width: 1199px) {
      padding-left: 5rem;
    }
  }

  .containerImage {
    img {
      position: absolute;
      width: 45rem;
      height: 852px;
      top: 0;
      transform: translate(4rem, -9rem);

      @media (max-width: 1680px) {
        width: 34rem;
        height: 793px;
        object-fit: cover;
        object-position: center;
        transform: translate(-1rem, -6rem);
      }

      @media (max-width: 1199px) {
        width: 28rem;
        transform: translate(-1rem, -8rem);
      }

      @media (max-width: 1023px) {
        width: 100%;
        height: 100%;
        transform: none;
        position: relative;
      }
    }

    @media (max-width: 1023px) {
      text-align: center;
      margin-top: 6rem;
      padding-bottom: 2rem;
    }

    @media (max-width: 767px) {
      margin-top: 7rem;
    }
  }

  @media (max-width: 1199px) {
    margin-top: 10rem;
  }
`;

export const Title = styled.div`
  text-align: center;
  margin-top: 14rem;
  h2 {
    font-family: "Gotham Bold";
    font-size: 3.3rem;
    text-align: center;
    padding-right: 15px;
    padding-left: 15px;
  }
`;
