import { Grid } from "@mui/material";
import { useRouter } from "next/router";
import { Container } from "../../../styles/global";
import {
  Section,
  ContainerTitle,
  ContainerImage,
  ImageTop,
  ImageBottomLeft,
  ImageBottomRight,
} from "./style";

export default function Botafogo() {
  const router = useRouter();

  return (
    <Section>
      <Container>
        <ContainerTitle>
          <h2>Botafogo</h2>
          <p>Um bairro que valoriza o &quot;nós&quot;.</p>
        </ContainerTitle>
        <ContainerImage>
          <Grid container>
            <Grid item xs={12} sm={8} className="containerLeft">
              <img
                src="/images/home/botafogo-1.jpg"
                width="100%"
                alt="Família"
              />
              <ImageTop>
                <img
                  src="/images/home/botafogo-2.jpg"
                  width="100%"
                  alt="Pessoas correndo"
                />
              </ImageTop>
              <ImageBottomLeft>
                <img src="/images/home/botafogo-3.jpg" width="100%" alt="Bar" />
              </ImageBottomLeft>
              <ImageBottomRight>
                <img
                  src="/images/home/botafogo-4.jpg"
                  width="100%"
                  alt="Loja"
                />
              </ImageBottomRight>
            </Grid>
            <Grid item xs={12} sm={4} className="containerRight">
                <h6>we live.</h6>
                <h6>we work.</h6>
                <h6>we play.</h6>

                <button onClick={() => router.push('/contato')}>
                    Conheça mais
                    <img src="/images/home/icons/botafogo-arrow.png" width="auto" alt="Seta" />
                </button>
            </Grid>
          </Grid>
        </ContainerImage>
      </Container>
    </Section>
  );
}
