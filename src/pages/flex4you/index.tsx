import { useRouter } from "next/router";
import Head from "next/head";
import { Grid } from "@mui/material";

import Banner from "../../components/Banner";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import { Button } from "../../components/Button";

import { Container } from "../../styles/global";
import {
  Box,
  ContainerHeader,
  ContainerInformation,
  Logo,
  Section,
  Valores,
  FooterBox,
  ContainerFlex4You,
  Title,
} from "../../styles/pages/flex4you/style";

import { SplideCarousel } from "../../components/Splide";

export default function Flex4you() {
  const router = useRouter();
  const items = [
    { src: "/images/plantas/cob-701.jpg", reference: "flex4you-plantas" },
    {
      src: "/images/plantas/cob-702-703-deb.jpg",
      reference: "flex4you-plantas",
    },
    { src: "/images/plantas/cob-702-e-703.jpg", reference: "flex4you-plantas" },
    { src: "/images/plantas/apto-01-op01.jpg", reference: "flex4you-plantas" },
    { src: "/images/plantas/apto-01-op02.jpg", reference: "flex4you-plantas" },
    { src: "/images/plantas/apto-01-op03.jpg", reference: "flex4you-plantas" },
    { src: "/images/plantas/apto-01-op05.jpg", reference: "flex4you-plantas" },
    { src: "/images/plantas/apto-02-op01.jpg", reference: "flex4you-plantas" },
    { src: "/images/plantas/apto-02-op02.jpg", reference: "flex4you-plantas" },
    { src: "/images/plantas/apto-02-op03.jpg", reference: "flex4you-plantas" },
    { src: "/images/plantas/apto-02-op05.jpg", reference: "flex4you-plantas" },
    { src: "/images/plantas/apto-04-op01.jpg", reference: "flex4you-plantas" },
    { src: "/images/plantas/apto-04-op02.jpg", reference: "flex4you-plantas" },
    { src: "/images/plantas/apto-04-op03.jpg", reference: "flex4you-plantas" },
    { src: "/images/plantas/apto-04-op05.jpg", reference: "flex4you-plantas" },
  ];

  return (
    <>
      <Head>
        <title>Apartamento à venda em Botafogo | 3 e 4 quartos | GAFISA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Site da imobiliária performance" />
        <meta
          name="description"
          content="Receba mais informações sobre o lançamento da Gafisa no bairro de Botafogo, Rio de Janeiro. Clique e saiba mais."
        />
        <meta name="author" content="Internit" />
        <meta
          name="keywords"
          content="we sorocaba, gafisa, apartamento botafogo"
        />
      </Head>
      <Header showLogo={true} />
      <Banner page="Flex 4 You" />
      <Section>
        <Container>
          <ContainerHeader>
            <div className="title">
              <h4>Para se adaptar ao mundo de hoje</h4>
              <h2>tem que ser flexível</h2>
            </div>
            <div className="description">
              <p>
                No We Sorocaba, todos os apartamentos são Flex 4 you. É a opção
                que você tem de adaptar o seu 4 quartos ao estilo de vida da sua
                família. Você escolhe se quer uma sala maior, um escritório para
                o seu home office, ou mais suítes. São muitas as variações de
                plantas e acabamentos para você viver à sua maneira.
              </p>
            </div>
          </ContainerHeader>
          <ContainerInformation>
            <Grid container spacing={2}>
              <Grid item md={6} xs={12} className="containerImage">
                <img
                  src="/images/flex4you/people.png"
                  width="auto"
                  alt="Imagem de pessoas"
                />
              </Grid>
              <Grid item md={6} xs={12} className="containerDescription">
                <Box>
                  <Logo>
                    <img
                      src="/images/header/logo.png"
                      width="auto"
                      alt="Logo"
                    />
                  </Logo>
                  <Valores>
                    <span>Apartamentos</span>
                    <h5>109m² à 133m²</h5>
                    <span>Coberturas</span>
                    <h5>209m² à 262m²</h5>
                  </Valores>
                  <FooterBox>
                    <span>A planta ideal para cada um de nós.</span>
                  </FooterBox>
                </Box>
              </Grid>
            </Grid>
          </ContainerInformation>
        </Container>
        <ContainerFlex4You>
          <Box>
            <Container>
              <Grid container spacing={2}>
                <Grid item md={7} xs={12} className="containerText">
                  <div className="logo">
                    <img
                      src="/images/flex4you/logo-flex4you.png"
                      width="auto"
                      alt="Logo Flex 4 You"
                    />
                  </div>
                  <div className="description">
                    <h3>Original 4 quartos.</h3>
                    <p>
                      Tão original que pode se transformar em diversas plantas
                      diferentes.
                    </p>
                    <span>
                      A exclusividade de apenas 25 apartamentos com 2 vagas na
                      garagem*.
                    </span>
                  </div>
                </Grid>
                <Grid item md={5} xs={12} className="containerImage">
                  <img
                    src="/images/flex4you/fachada-lateral.jpg"
                    width="auto"
                    alt="Fachada Lateral"
                  />
                </Grid>
              </Grid>
            </Container>
          </Box>
        </ContainerFlex4You>
        <Title>
          <h2>Uma planta, diversas opções.</h2>
        </Title>
        <SplideCarousel
          nameSlide="flex4you"
          items={items}
          description={false}
          magnifying={false}
        />
        <div className="button">
          <Button onClick={() => router.push("/contato")}>
            solicite mais informações
          </Button>
        </div>
      </Section>
      <Footer showLogos={true} information={true} />
    </>
  );
}
