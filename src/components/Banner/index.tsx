import { Container } from "../../styles/global";

import {
  ApartmentInformations,
  Apartments,
  Box,
  ContainerInformation,
  Section,
  Selo,
  ContainerTitle,
} from "./style";

interface BannerProps {
  page: string;
}

export default function Banner({ page }: BannerProps) {
  return (
    <Section page={page}>
      <Container>
        {page === "Home" && (
          <>
            <ContainerInformation>
              <Box>
                <h2>
                  <strong>3 e 4 quartos</strong> mais
                  <br />
                  flexíveis de <strong>botafogo</strong>
                </h2>
              </Box>
              <ApartmentInformations>
                <Apartments>
                  <p>Apartamentos</p>
                  <h6>109 a 133m²</h6>
                </Apartments>
                <Apartments>
                  <p>Coberturas duplex e lineares</p>
                  <h6>220 a 262m²</h6>
                </Apartments>
              </ApartmentInformations>

              <span>Apenas 25 unidades.</span>
            </ContainerInformation>
            <Selo>
              <img
                src="/images/home/right-round-img.png"
                width="auto"
                alt="Selo apartamento"
              />
            </Selo>
          </>
        )}
        {page !== "Home" && (
          <ContainerTitle>
            <h1>{page}</h1>
          </ContainerTitle>
        )}
      </Container>
    </Section>
  );
}
