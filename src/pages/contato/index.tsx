import { useRef } from "react";
import Head from "next/head";
import Formsy from "formsy-react";
import CircularProgress from "@mui/material/CircularProgress";

import Banner from "../../components/Banner";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import { Container } from "../../styles/global";
import { Snackbar, Grid } from "@mui/material";
import sendToHypnobox from "../../utils/hypnobox";
import sendToCRM from "../../utils/novoCrm";

import {
  Section,
  ContainerInformation,
  Information,
  Logo,
  Box,
  Image,
  Title,
  ContainerForm,
  ContainerPolitica,
} from "../../styles/pages/contato/style";

import {
  CheckboxFormsy,
  InputMaskFormsy,
  TextFieldFormsy,
} from "../../components/formsy";
import { useState } from "react";
import { useSnackbar } from "notistack";
import Link from "next/link";
import { PoliticaDePrivacidadeModal } from "../../components/PoliticaDePrivacidadeModal";

interface ModelProps {
  email: string;
  mensagem: string;
  name: string;
  phone: string;
}

export default function Contato() {
  const { enqueueSnackbar } = useSnackbar();
  const [isFormValid, setIsFormValid] = useState(false);
  const [loading, setLoading] = useState(false);
  const [openPolitica, setOpenPolitica] = useState(false);
  const [checkboxPolicy, setCheckboxPolicy] = useState(false);
  const formRef = useRef<Formsy>(null);

  async function processForm(model: ModelProps) {
    setLoading(true);
    const DDD = model.phone.split(" ")[0].replace(/[^a-z0-9]/gi, "");
    const phone = model.phone.split(" ")[1].replace(/[^a-z0-9]/gi, "");

    const responseNovoCrm = await sendToCRM(
      "215",
      model.name,
      model.email,
      DDD,
      phone,
      model.mensagem,
      urlParam("utm_source"),
      urlParam("utm_medium"),
      urlParam("utm_campaign")
    );

    const responseHypnobox = await sendToHypnobox(
      "647",
      model.name,
      model.email,
      DDD,
      phone,
      model.mensagem,
      "organico",
      urlParam("utm_source"),
      urlParam("utm_medium"),
      urlParam("utm_campaign")
    );

    setLoading(false);
    enqueueSnackbar(
      responseHypnobox.status === 200 || responseNovoCrm.status === 200
        ? "Mensagem enviada com successo!"
        : "Erro ao enviar mensagem!",
      {
        variant:
          responseHypnobox.status === 200 || responseNovoCrm.status === 200
            ? "success"
            : "error",
      }
    );

    formRef.current?.reset();
  }

  function urlParam(name: string) {
    var results = new RegExp("[?&]" + name + "=([^&#]*)").exec(
      window.location.href
    );
    if (results == null) {
      return "Acesso Direto";
    } else {
      return decodeURI(results[1]) || "";
    }
  }

  const handleClose = () => setOpenPolitica(false);

  return (
    <>
      <Head>
        <title>Apartamento à venda em Botafogo | 3 e 4 quartos | GAFISA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Site da imobiliária performance" />
        <meta
          name="description"
          content="Receba mais informações sobre o lançamento da Gafisa no bairro de Botafogo, Rio de Janeiro. Clique e saiba mais."
        />
        <meta name="author" content="Internit" />
        <meta
          name="keywords"
          content="we sorocaba, gafisa, apartamento botafogo"
        />
      </Head>
      <Header showLogo={true} />
      <Banner page="Contato" />
      <Section>
        <Container>
          <Grid container spacing={2}>
            <Grid item md={6} xs={12}>
              <ContainerInformation>
                <Logo>
                  <img
                    src="/images/logo-branca.png"
                    width="auto"
                    alt="We Sorocaba"
                  />
                </Logo>
                <Information>
                  <Box>
                    <a
                      href="https://goo.gl/maps/zDZhGQrptvjHdMRM6"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <span>Rua Sorocaba, 701 - Botafogo</span>
                    </a>
                  </Box>
                  <h4>
                    Entre em contato no formulário ao lado para saber mais.
                  </h4>
                </Information>
              </ContainerInformation>
            </Grid>
            <Grid item md={6} xs={12} className="containerRight">
              <Title>
                <h2>Solicite mais</h2>
                <span>informações</span>
              </Title>
              <ContainerForm>
                <Formsy
                  onValidSubmit={processForm}
                  onValid={() => setIsFormValid(true)}
                  onInvalid={() => setIsFormValid(false)}
                  ref={formRef}
                  id="form-contato-principal"
                >
                  <Grid item xs={12}>
                    <Grid container>
                      <Grid item xs={12}>
                        <TextFieldFormsy
                          name="name"
                          type="text"
                          label="Nome completo"
                          variant="standard"
                          required
                        />
                      </Grid>
                      <Grid item xs={12} marginTop={2}>
                        <TextFieldFormsy
                          name="email"
                          type="email"
                          label="E-mail"
                          variant="standard"
                          required
                        />
                      </Grid>
                      <Grid item xs={12} marginTop={2}>
                        <InputMaskFormsy
                          name="phone"
                          type="tel"
                          label="Telefone com DDD"
                          variant="standard"
                          mask="(99) 99999-9999"
                          required
                        />
                      </Grid>
                      <Grid item xs={12} marginTop={2}>
                        <TextFieldFormsy
                          type="textarea"
                          name="mensagem"
                          variant="standard"
                          label="Mensagem"
                          multiline
                          rows={6}
                          required
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <ContainerPolitica>
                          <CheckboxFormsy value={checkboxPolicy} onChange={() => setCheckboxPolicy(!checkboxPolicy)} name="aceite" label="" required />
                          <span>
                            Li e aceito os termos e a{" "}
                            <strong onClick={() => setOpenPolitica(true)}>
                              Política de Privacidade.
                            </strong>
                          </span>
                        </ContainerPolitica>
                      </Grid>
                      <Grid item xs={12} marginTop={2}>
                        <button disabled={!isFormValid || !checkboxPolicy}>
                          {loading ? (
                            <CircularProgress color="inherit" size="1.5rem" />
                          ) : (
                            "enviar"
                          )}
                        </button>
                      </Grid>
                    </Grid>
                  </Grid>
                </Formsy>
              </ContainerForm>
            </Grid>
          </Grid>
        </Container>
      </Section>
      <PoliticaDePrivacidadeModal open={openPolitica} handleClose={handleClose} />
      <Footer showLogos={true} information={false} />
    </>
  );
}
