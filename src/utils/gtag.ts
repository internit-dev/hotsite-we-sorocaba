export const GA_TRACKING_ID = "UA-147258550-1";

// https://developers.google.com/analytics/devguides/collection/gtagjs/pages
export const pageview = (url: URL): void => {
  window.gtag("config", GA_TRACKING_ID);
};

type GTagEvent = {
  action: string;
  category: string;
  label: string;
};

// https://developers.google.com/analytics/devguides/collection/gtagjs/events
export const event = ({ action, category, label }: GTagEvent): void => {
  window.gtag("event", action, {
    event_category: category,
    event_label: label,
  });
};