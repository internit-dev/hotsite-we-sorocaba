import styled from "styled-components";

export const Section = styled.section`
  margin: 6rem 0;
`;

export const ContainerHeader = styled.div`
  padding-left: 9rem;
  margin-bottom: 6rem;
  position: relative;

  &::before {
    position: absolute;
    content: "";
    background: url(/images/localizacao/blocket-left.svg);
    width: 96px;
    height: 538px;
    top: -5rem;
    left: 2rem;

    @media (max-width: 1023px) {
      left: 1rem;
    }

    @media (max-width: 767px) {
      left: 0;
      width: 56px;
      height: 322px;
      background-size: contain;
      top: -3rem;
    }
  }

  &::after {
    position: absolute;
    content: "";
    background: url(/images/localizacao/blocket-right.svg);
    width: 35.69px;
    height: 200px;
    top: 0;
    right: 44rem;

    @media (max-width: 1919px){
      right: 35rem;
    }

    @media (max-width: 1280px) {
      top: -2rem;
      right: 16rem;
    }

    @media (max-width: 1023px) {
      right: 5rem;
    }

    @media (max-width: 767px) {
      right: 1rem;
    }
  }

  @media (max-width: 767px) {
    padding-left: 3rem;
  }
`;

export const Title = styled.div`
  h2 {
    font-size: 4rem;
    font-family: "Gotham Medium";
    margin-bottom: 0;
    @media (max-width: 1280px) {
      margin-bottom: 1rem;
    }

    @media (max-width: 767px) {
      font-size: 3.5rem;
    }
  }

  p {
    font-size: 2.1rem;
    position: relative;
    width: 40%;

    span {
      position: absolute;
      font-family: "Bodoni MT";
      color: ${({ theme }) => theme.grena};
      font-size: 3.6rem;
      right: 0;
      top: -1.5rem;

      strong {
        color: ${({ theme }) => theme.black};
      }

      @media (max-width: 767px) {
        position: relative;
        top: 0;
      }
    }

    @media (max-width: 1919px){
      width: 46%;
    }

    @media (max-width: 1280px) {
      width: 60%;
    }

    @media (max-width: 1023px) {
      width: 80%;
    }

    @media (max-width: 767px) {
      font-size: 1.8rem;
      width: 100%;
    }
  }
`;

export const ContainerImage = styled.div`
  img {
    @media (max-width: 767px) {
      height: 220px;
      object-fit: cover;
      object-position: 31%;
    }
  }
`;

export const ContainerLocation = styled.div`
  margin-top: 7.5rem;
  padding-top: 6rem;
  position: relative;

  h6 {
    font-size: 2.7rem;
    font-weight: 100;
    position: absolute;
    top: 0;
    right: 3rem;

    &::before {
      position: absolute;
      content: "";
      background: url(/images/localizacao/colcheteleft.png);
      background-size: contain;
      width: 14px;
      height: 75px;
      left: -2rem;
      font-size: 2rem;
      top: -0.7rem;

      @media (max-width: 767px) {
        top: -1.5rem;
      }
    }

    &::after {
      position: absolute;
      content: "";
      background: url(/images/localizacao/colcheteright.png);
      background-size: contain;
      width: 14px;
      height: 75px;
      right: -2rem;
      top: -0.7rem;
      font-size: 2rem;

      @media (max-width: 767px) {
        top: -1.5rem;
      }
    }

    @media (max-width: 767px) {
      font-size: 2rem;
    }
  }

  .description {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }

  .containerRight {
    display: flex;
    align-items: flex-start;
    justify-content: flex-start;
    flex-direction: column;
    padding-top: 11rem;

    button {
      margin-left: 6rem;
      font-size: 1.3rem;

      @media (max-width: 1023px) {
        margin-left: 1rem;
      }

      @media (max-width: 767px) {
        margin-left: 2rem;
      }
    }

    @media (max-width: 1280px) {
      padding-top: 2rem;
    }

    @media (max-width: 1023px) {
      padding-top: 4rem;
    }
  }

  @media (max-width: 1280px) {
    margin-top: 5.5rem;
    padding-top: 10rem;
  }
`;

export const ContainerInformation = styled.div`
  padding-left: 5rem;
  width: 100%;

  @media (max-width: 1023px) {
    padding-left: 0;
  }
`;

export const Information = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 1rem;
  margin-right: 1rem;
  border-bottom: 2px solid rgba(128, 106, 37, 0.7);

  p {
    font-size: 1.4rem;
    margin-bottom: 0;
  }

  & + div {
    margin-top: 1.2rem;
  }
`;

export const ContainerWeLive = styled.div`
  margin-top: 4rem;
`;

export const ContainerWeLiveTitle = styled.div`
  display: flex;
  margin-bottom: 2rem;
  padding-left: 6rem;

  @media (max-width: 1023px) {
    padding-left: 4rem;
  }

  @media (max-width: 767px) {
    flex-direction: column;
  }
`;

export const WeLiveTitle = styled.div`
  h2 {
    font-family: "Bodoni MT";
    font-size: 4rem;
    line-height: 57px;
  }

  @media (max-width: 1023px) {
    width: 90%;
  }
`;

export const WeLiveSubTitle = styled.div`
  display: flex;
  align-items: center;
  margin-left: 6rem;
  h6 {
    font-family: "Gotham Light";
    font-size: 1.6rem;
    width: 50%;

    @media (max-width: 1280px) {
      width: 85%;
    }

    @media (max-width: 1023px) {
      width: 100%;
    }
  }

  @media (max-width: 767px) {
    margin-left: 0;
    margin-top: 2rem;
  }
`;

export const WeLiveImages = styled.div`
  .containerImagesLeft {
    > div {
      &:not(:first-child) {
        margin-top: 2.1rem;
      }
    }
  }
`;

export const Text = styled.div`
  margin-top: 3.1rem;
  display: flex;
  flex-direction: column;
  width: 50%;
  h3 {
    font-family: "Bodoni MT";
    font-size: 4rem;
    line-height: 57px;
    text-align: right;
  }

  @media (max-width: 1023px) {
    width: 60%;
  }

  @media (max-width: 767px) {
    width: 75%;
  }
`;
