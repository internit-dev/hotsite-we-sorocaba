import styled from "styled-components";

export const Footer = styled.footer`
  margin-top: 4rem;
  > div {
    .containerLeft {
      text-align: right;
      padding-right: 3rem;

      @media (max-width: 767px) {
        padding-right: 0;
        text-align: center;
      }
    }

    .containerRight {
      @media (max-width: 767px) {
        text-align: center;
      }
    }

    img {
      width: auto;
      @media (max-width: 767px) {
        width: 80%;
      }
    }
  }

  .popUpPrivacyPolicy {
    > div {
      background: ${({ theme }) => theme.black};
      color: ${({ theme }) => theme.white};

      strong {
        cursor: pointer;
      }

      svg {
        color: ${({ theme }) => theme.white};
      }

      button {
        color: ${({ theme }) => theme.black};
        background: ${({ theme }) => theme.white};
        margin-top: 1rem;
      }
    }

    @media (max-width: 1024px) {
      width: 95%;
    }
  }

  .whatsapp {
    position: fixed;
    bottom: 1.2rem;
    right: 1.2rem;
    z-index: 9999;
    cursor: pointer;
    animation: animate-whatsapp 1s infinite alternate;
  }

  @keyframes animate-whatsapp {
    0% {
      transform: translateY(-.8rem);
    }
    100%{
      transform: translateY(-1.8rem);
    }
  }
`;

export const ContainerFooter = styled.div`
  background-color: ${({ theme }) => theme.black};
  color: ${({ theme }) => theme.white};
  display: flex;
  align-items: center;
  padding: 1rem;

  p {
    margin-bottom: 0;
    display: flex;
    align-items: center;

    img {
      margin-left: 1rem;

      @media (max-width: 767px) {
        width: auto;
      }
    }

    @media (max-width: 767px) {
      width: 100%;
      justify-content: center;

      strong {
        margin-left: 0 !important;
      }
    }
  }

  .policy {
    p {
      @media (max-width: 767px) {
        font-size: 0.8rem;
        display: block;
      }
    }
  }

  .copyright {
    justify-content: flex-end;
    @media (max-width: 767px) {
      justify-content: flex-start;
      margin-top: 1rem;
    }

    p {
      font-size: 0.8rem;
    }

    a {
      img {
        &.incena {
          width: 71% !important;

          @media (max-width: 767px) {
            width: 60% !important;
          }
        }

        @media (max-width: 767px) {
          width: 60%;
          margin-left: 0;
        }
      }
    }
  }

  @media (max-width: 767px) {
    text-align: center;
  }
`;
