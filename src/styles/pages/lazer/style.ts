import styled from "styled-components";

export const Section = styled.section`
  margin: 3rem 0;

  > div {
    &.button {
      width: 100%;
      text-align: center;
    }
  }

  .swiper-slide {
    img {
      border-radius: 1rem;
      height: 405px;
      object-fit: cover;
      object-position: center;
    }
  }

  .prev {
    left: 38rem !important;
    svg {
      width: 3rem !important;
      height: 3rem !important;
    }

    @media (min-width: 1280px) and (max-width: 1919px) {
      left: 28rem !important;
    }
    @media (max-width: 1279px) {
      left: 22rem !important;
    }

    @media (max-width: 1023px) {
      left: 17rem !important;
    }

    @media (max-width: 767px) {
      left: 1rem !important;
    }
  }

  .next {
    right: 38rem !important;

    svg {
      width: 3rem !important;
      height: 3rem !important;
    }

    @media (min-width: 1280px) and (max-width: 1919px) {
      right: 28rem !important;
    }
    @media (max-width: 1279px) {
      right: 22rem !important;
    }

    @media (max-width: 1023px) {
      right: 17rem !important;
    }

    @media (max-width: 767px) {
      right: 1rem !important;
    }
  }
`;

export const ContainerTitle = styled.div`
  margin-bottom: 2rem;

  h2 {
    font-family: "Gotham Bold";
    font-size: 3.3rem;
    line-height: 52px;
    margin-bottom: 1rem;

    @media (max-width: 767px) {
      font-size: 2.3rem;
      line-height: 39px;
    }
  }

  span {
    font-size: 1.7rem;

    @media (max-width: 767px) {
      font-size: 1.5rem;
    }
  }
`;

export const ContainerMapa = styled.div`
  text-align: center;

  .containerImage {
    img {
      @media (max-width: 1319px) {
        max-width: 100%;
      }
    }
  }

  button {
    font-size: 1.3rem;
    color: #6a072f;
  }
`;

export const Box = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
`;

export const Option = styled.div`
  display: flex;

  .number {
    min-width: 42px;
    height: 40px;
    background-color: ${({ theme }) => theme.grena};
    display: flex;
    justify-content: center;
    align-items: center;
    margin-right: 0.9rem;

    span {
      color: ${({ theme }) => theme.white};
      font-family: "Gotham Light";
      font-weight: 600;
    }
  }

  .name {
    display: flex;
    justify-content: center;
    flex-direction: column;

    span {
      font-size: 1.2rem;
      font-weight: 600;
    }

    p {
      font-size: 0.9rem;
      font-style: italic;
      margin-top: 0.6rem;
      line-height: 19px;
      text-align: left;
    }
  }

  &:not(:first-child) {
    margin-top: 1.2rem;
  }

  &:first-child {
    @media (max-width: 767px) {
      margin-top: 1.2rem;
    }
  }
`;

export const ContainerSlide = styled.div``;
