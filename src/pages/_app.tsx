import { useEffect } from 'react';
import type { AppProps } from "next/app";
import { useRouter } from "next/router";
import { ThemeProvider } from "styled-components";
import { SnackbarProvider } from 'notistack';

import theme from "../styles/theme";
import GlobalStyle from "../styles/global";
import { ToggleSideBarContext } from "../hooks/sidebar";

import * as gtag from "../utils/gtag";

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();

  const isProduction = true;

  useEffect(() => {
    const handleRouteChange = (url: URL) => {
      /* invoke analytics function only for production */
      if (isProduction) gtag.pageview(url);
    };
    router.events.on("routeChangeComplete", handleRouteChange);
    return () => {
      router.events.off("routeChangeComplete", handleRouteChange);
    };
  }, [router.events]);

  return (
    <ToggleSideBarContext>
      <ThemeProvider theme={theme}>
        <SnackbarProvider maxSnack={3} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
          <Component {...pageProps} />
          <GlobalStyle />
        </SnackbarProvider>
      </ThemeProvider>
    </ToggleSideBarContext>
  );
}

export default MyApp;
