import styled from "styled-components";

import HomeImg from "../../assets/images/Banner/home.jpg";
import EmpreendimentoImg from "../../assets/images/Banner/empreendimento.jpg";
import Flex4youImg from "../../assets/images/Banner/flex-4-you.jpg";
import LazerImg from "../../assets/images/Banner/lazer.jpg";
import LocalizacaoImg from "../../assets/images/Banner/localizacao.jpg";
import RealizacaoImg from "../../assets/images/Banner/realizacao.jpg";
import ViverBemImg from "../../assets/images/Banner/viverBem.jpg";
import ContatoImg from "../../assets/images/Banner/contato.jpg";
import BracketLeft from "../../assets/images/Banner/white-bracket-left.png";
import BracketRight from "../../assets/images/Banner/white-bracket-right.png";

interface SectionProps {
  page: string | undefined;
}

const selectedImageByProps = (value: string) => {
  switch (value) {
    case "Empreendimento":
      return EmpreendimentoImg.src;
    case "Flex 4 You":
      return Flex4youImg.src;
    case "Lazer":
      return LazerImg.src;
    case "Localização":
      return LocalizacaoImg.src;
    case "Realização":
      return RealizacaoImg.src;
    case "Viver Bem":
      return ViverBemImg.src;
    case "Contato":
      return ContatoImg.src;
    default:
      return HomeImg.src;
  }
};

const selectedPercentBackground = (value: string) => {
  switch (value) {
    case "Empreendimento":
      return "42%";
    case "Flex 4 You":
      return "60%";
    case "Lazer":
      return "18%";
    case "Realização":
      return "46%";
    case "Viver Bem":
      return "70%";
    case "Localização":
      return "55%";
    default:
      break;
  }
};

export const Section = styled.section<SectionProps>`
  width: 100%;
  min-height: -webkit-fill-available;
  height: ${(props) => (props.page !== "Home" ? "37.5rem" : "100vh")};

  background: url("${(props) =>
    props.page ? selectedImageByProps(props.page) : ""}");
  background-repeat: no-repeat !important;
  background-size: cover !important;

  position: relative;
  overflow: hidden;

  @media (max-width: 850px) and (orientation: landscape) {
    height: 100%;
  }

  > div {
    height: 100%;
  }

  @media (max-width: 767px) {
    background-position: ${(props) =>
      props?.page ? selectedPercentBackground(props.page) : "0%"};
  }
`;

export const ContainerInformation = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  flex-direction: column;
  color: ${({ theme }) => theme.white};
  padding-top: 3rem;

  span {
    margin-top: 3rem;
    font-size: 1.3rem;
  }
`;

export const Box = styled.div`
  h2 {
    font-size: 3rem;
    line-height: 1rem;
    padding-bottom: 4rem;
    line-height: 65px;
    text-transform: uppercase;
    font-family: "Gotham Light";
    color: ${({ theme }) => theme.white};

    @media (max-width: 767px) {
      padding-bottom: 1rem;
      font-size: 2.5rem;
      line-height: 46px;
    }
  }
`;

export const ApartmentInformations = styled.div`
  margin-top: 3rem;
  display: flex;
  align-items: center;
  position: relative;
  margin-left: 2rem;

  &::before {
    position: absolute;
    content: "";
    background: url(${BracketLeft.src});
    background-size: contain;
    width: 23px;
    height: 121px;
    top: -1rem;
    left: -2rem;

    @media (max-width: 767px) {
      top: -.8rem;
      left: -2.5rem;
    }
  }

  &::after {
    position: absolute;
    content: "";
    background: url(${BracketRight.src});
    background-size: contain;
    width: 23px;
    height: 121px;
    top: -1rem;
    left: 31rem;

    @media (max-width: 767px) {
      top: -.8rem;
      left: unset;
      right: -.5rem;
    }
  }
`;

export const Apartments = styled.div`
  & + div {
    margin-left: 2rem;

    @media (max-width: 767px) {
      margin-left: 1rem;
    }
  }

  p,
  h6 {
    font-family: "Gotham Regular";
    line-height: calc(2 * 1rem);
    margin-bottom: 0;
    color: ${({ theme }) => theme.white};
  }

  p {
    font-family: "Gotham Light";
    font-size: 1.3rem;
    margin-bottom: 1.3rem;
  }

  h6 {
    font-size: 1.8rem;

    @media (max-width: 767px) {
      font-size: 1.5rem;
    }
  }
`;

export const Selo = styled.div`
  position: absolute;
  bottom: 1rem;
  right: 1rem;
  text-align: right;

  img {
    width: 50%;

    @media (max-width: 767px) {
      width: 20%;
    }
  }
`;

export const ContainerTitle = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

  h1 {
    color: ${({ theme }) => theme.white};
    font-size: 4.6rem;
    font-family: "Gotham Medium";
    display: flex;
    align-items: center;
    position: relative;

    &::before {
      position: absolute;
      content: "[";
      color: rgba(128, 106, 37, 0.7);
      font-size: 9.3rem;
      font-weight: 500;
      left: -6rem;
      top: -3.6rem;

      @media (max-width: 1199px) {
        font-size: 6.3rem;
        left: -4rem;
        top: -2rem;
      }

      @media (max-width: 1023px) {
        font-size: 7.5rem;
        left: -2rem;
        top: -2rem;
      }

      @media (max-width: 767px) {
        font-size: 5.5rem;
        left: -2rem;
      }
    }

    &::after {
      position: absolute;
      content: "]";
      color: rgba(128, 106, 37, 0.7);
      font-size: 9.3rem;
      font-weight: 500;
      right: -6rem;
      top: -3.6rem;

      @media (max-width: 1023px) {
        font-size: 7.5rem;
        right: -4rem;
        top: -2rem;
      }

      @media (max-width: 767px) {
        font-size: 5.5rem;
        right: -2rem;
        top: -2rem;
      }

      @media (max-width: 550px) {
        right: -2rem;
      }
    }

    @media (max-width: 767px) {
      font-size: 3rem;
    }

    @media (max-width: 767px) {
      font-size: 2.3rem;
    }
  }

  @media (max-width: 767px) {
    top: 58%;
  }
`;
