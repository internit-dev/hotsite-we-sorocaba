import { useRouter } from "next/router";

import { Button } from "../../Button";
import { SplideCarousel } from "../../Splide";
import { Section, ContainerPlantas, Title } from "./style";

export default function Planta() {
  const router = useRouter();
  const items = [
    { src: "/images/plantas/cob-701.jpg", reference: "Home-plantas" },
    { src: "/images/plantas/cob-702-703-deb.jpg", reference: "Home-plantas" },
    { src: "/images/plantas/cob-702-e-703.jpg", reference: "Home-plantas" },
    { src: "/images/plantas/apto-01-op01.jpg", reference: "Home-plantas" },
    { src: "/images/plantas/apto-01-op02.jpg", reference: "Home-plantas" },
    { src: "/images/plantas/apto-01-op03.jpg", reference: "Home-plantas" },
    { src: "/images/plantas/apto-01-op05.jpg", reference: "Home-plantas" },
    { src: "/images/plantas/apto-02-op01.jpg", reference: "Home-plantas" },
    { src: "/images/plantas/apto-02-op02.jpg", reference: "Home-plantas" },
    { src: "/images/plantas/apto-02-op03.jpg", reference: "Home-plantas" },
    { src: "/images/plantas/apto-02-op05.jpg", reference: "Home-plantas" },
    { src: "/images/plantas/apto-04-op01.jpg", reference: "Home-plantas" },
    { src: "/images/plantas/apto-04-op02.jpg", reference: "Home-plantas" },
    { src: "/images/plantas/apto-04-op03.jpg", reference: "Home-plantas" },
    { src: "/images/plantas/apto-04-op05.jpg", reference: "Home-plantas" },
  ];

  return (
    <Section>
      <ContainerPlantas>
        <Title>
          <h2>Uma planta, diversas opções.</h2>
          <p>No We Sorocaba, todos os apartamentos são Flex 4 you. É a opção que você tem de adaptar o seu 4 quartos ao estilo de vida da sua família. Você escolhe se quer uma sala maior, um escritório para o seu home office, ou mais suítes. São muitas as variações de plantas e acabamentos para você viver à sua maneira.</p>
        </Title>
      </ContainerPlantas>
      <SplideCarousel nameSlide="plantas" description={false} items={items} magnifying={true} />
      <Button onClick={() => router.push('/flex4you')}>saiba mais</Button>
    </Section>
  );
}
