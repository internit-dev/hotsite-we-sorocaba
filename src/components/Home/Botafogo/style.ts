import styled from "styled-components";

export const Section = styled.section`
  margin: 13rem 0;
`;

export const ContainerTitle = styled.div`
  h2 {
    font-family: "Gotham Medium";
    font-size: 4rem;
    margin-bottom: 2rem;
    position: relative;
    margin-left: 1.2rem;

    &::before {
      position: absolute;
      content: "";
      background: url(/images/home/brown-bracket-left.png);
      background-size: contain;
      width: 19px;
      height: 106px;
      top: -1rem;
      left: -1rem;
      @media (max-width: 767px) {
        height: 105px;
      }
    }

    &::after {
      position: absolute;
      content: "";
      background: url(/images/home/brown-bracket-right.png);
      background-size: contain;
      width: 19px;
      height: 106px;
      top: -1rem;
      left: 19rem;

      @media (max-width: 767px) {
        height: 105px;
      }
    }

    @media (max-width: 767px) {
      margin-bottom: 3rem;
    }
  }

  p {
    font-size: 1.7rem;
  }
`;

export const ContainerImage = styled.div`
  margin-top: 4rem;

  .containerLeft {
    position: relative;

    img {
      width: 100%;
    }
  }

  .containerRight {
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding-left: 3rem;
    position: relative;

    h6 {
      font-family: "Gotham Light";
      font-weight: 500;
      font-size: 2.8rem;
      position: absolute;
      top: 50%;

      &:nth-child(2) {
        top: 65%;
        left: 15%;

        @media (max-width: 1023px) {
          top: 59%;
        }

        @media (max-width: 767px) {
          left: 0;
          text-align: center;
        }
      }

      &:nth-child(3) {
        top: 80%;
        left: 30%;

        @media (max-width: 767px) {
          left: 0;
          text-align: right;
        }
      }

      @media (max-width: 1023px) {
        top: 40%;
      }

      @media (max-width: 767px) {
        position: relative;
      }
    }

    button {
      position: absolute;
      bottom: -3rem;
      left: 5rem;
      background: transparent;
      border: none;
      display: flex;
      align-items: center;
      font-size: 1.3rem;
      transition: color 0.3s;

      img {
        margin-left: 1rem;
        width: 25%;
      }

      &:hover {
        color: ${({ theme }) => theme.green};
      }
      @media (max-width: 1023px) {
        font-size: 1.2rem;
        left: 3rem;
        bottom: -6rem;
      }

      @media (max-width: 767px) {
        font-size: 1.2rem;
        left: 50%;
        bottom: -13rem;
        transform: translateX(-50%);
      }
    }

    @media (max-width: 1023px) {
      padding-left: 1rem;
    }

    @media (max-width: 767px) {
      padding-top: 4rem;
    }
  }

  @media (max-width: 767px) {
    margin-top: 7rem;
  }
`;

export const ImageTop = styled.div`
  position: absolute;
  top: -4rem;
  right: -22rem;
  border: 10px solid ${({ theme }) => theme.white};
  width: 50%;
  height: auto;
  display: flex;

  @media (max-width: 1023px) {
    right: -12rem;
  }

  @media (max-width: 767px) {
    left: 1rem;
  }
`;

export const ImageBottomLeft = styled.div`
  position: absolute;
  bottom: -9rem;
  left: 3rem;
  border: 10px solid ${({ theme }) => theme.white};
  width: 40%;
  height: auto;
  display: flex;

  @media (max-width: 1023px) {
    bottom: -7rem;
    left: 1rem;
    width: 48%;
  }

  @media (max-width: 767px) {
    width: 45%;
    bottom: -6rem;
  }
`;

export const ImageBottomRight = styled.div`
  position: absolute;
  bottom: -3rem;
  right: -3rem;
  border: 10px solid ${({ theme }) => theme.white};
  width: 32%;
  height: auto;
  display: flex;

  @media (max-width: 1023px) {
    width: 37%;
  }

  @media (max-width: 767px) {
    right: 1rem;
  }
`;
