import styled from "styled-components";

export const Section = styled.section``;

export const Header = styled.div`
  margin-top: 3rem;

  h1 {
    font-family: "Gotham Medium";
    font-size: 3rem;
    margin-bottom: 0;
  }

  p {
    font-family: "Gotham Regular";
    font-size: 1.3rem;
    margin-bottom: 1rem;
    margin-top: 0.3rem;
  }
`;

export const ContainerImages = styled.div`
  .imageLeft {
    position: relative;

    p {
      color: ${({ theme }) => theme.white};
      text-transform: uppercase;
      font-size: 0.8rem;
      position: absolute;
      bottom: 2rem;
      right: 10rem;

      &::before {
        position: absolute;
        content: "[";
        left: -1rem;
        font-size: 2rem;
        top: -0.9rem;
        color: ${({ theme }) => theme.black};
        font-family: "Gotham Medium";
      }

      &::after {
        position: absolute;
        content: "]";
        right: -1rem;
        top: -0.9rem;
        font-size: 2rem;
        color: ${({ theme }) => theme.black};
        font-family: "Gotham Medium";
      }

      @media (max-width: 1199px) {
        right: 7rem;
      }
    }

    .image {
      width: 86%;
      > img {
        box-shadow: 3px 3px 6px 0px rgba(0, 0, 0, 0.3);
      }
    }
  }

  .imageRight {
    padding-left: 3rem;
    position: relative;

    img {
      box-shadow: 3px 3px 6px 0px rgba(0, 0, 0, 0.3);
    }

    @media (max-width: 767px) {
      margin-top: 2rem;
    }
  }

  .imageLeft,
  .imageRight {
    position: relative;
    .image {
      position: relative;
      cursor: pointer;
      .lupa {
        position: absolute;
        top: 45%;
        left: 50%;
        transform: translate(-50%);
        display: flex;
        justify-content: center;
        align-items: center;
        opacity: 0;
        transition: all ease 0.3s;

        img {
          width: 20%;
          box-shadow: none;
        }
      }

      &:hover {
        .lupa {
          opacity: 1;
        }
      }
    }
  }
`;

export const Address = styled.div`
  position: absolute;
  left: 50%;
  top: 84%;
  transform: translateX(-50%);
  width: 40%;
  p {
    color: ${({ theme }) => theme.black};
    font-size: 2.1rem;
    position: relative;
    text-align: center;

    &::before {
      position: absolute;
      content: "";
      background: url(/images/home/dark-bracket-left.png);
      background-size: contain;
      width: 12px;
      height: 63px;
      left: -1rem;
      top: -0.9rem;
    }

    &::after {
      position: absolute;
      content: "";
      background: url(/images/home/dark-bracket-right.png);
      background-size: contain;
      width: 12px;
      height: 63px;
      right: -1rem;
      top: -0.9rem;
    }

    @media (max-width: 767px){
      font-size: 1.7rem;
    }
  }

  @media (max-width: 1199px) {
    width: 60%;
  }

  @media (max-width: 1023px) {
    position: relative;
    top: 0;
    margin-top: 3rem;
    width: 44%;
    right: unset;
    transform: none;
  }

  @media (max-width: 767px) {
    width: 80%;
    left: 0;
  }
`;

export const ContainerText = styled.div`
  width: 48%;
  margin: 3rem auto 0 auto;
  text-align: center;

  p {
    line-height: 30px;
    font-size: 1.1rem;
    text-align: left;
  }

  button {
    margin-top: 4rem;
  }

  @media (max-width: 1199px) {
    width: 60%;
  }

  @media (max-width: 1023px) {
    width: 90%;
  }

  @media (max-width: 767px) {
    width: 100%;
    margin-top: 5rem;
  }
`;
