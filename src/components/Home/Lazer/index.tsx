import { useRouter } from "next/router";
import { Container } from "../../../styles/global";
import { Button } from "../../Button";
import { SplideCarousel } from "../../Splide";

import { Section, Title } from "./style";

export default function Lazer() {
  const router = useRouter();

  const items = [
    {
      src: "/images/lazer/piscina.jpg",
      description: "Piscina",
      reference: "home-lazer",
    },
    {
      src: "/images/lazer/jogos-teen.jpg",
      description: "Jogos Teen",
      reference: "home-lazer",
    },
    {
      src: "/images/lazer/academia.jpg",
      description: "Academia",
      reference: "home-lazer",
    },
    {
      src: "/images/lazer/brinquedoteca.jpg",
      description: "Brinquedoteca",
      reference: "home-lazer",
    },
    {
      src: "/images/lazer/salao-de-festas.jpg",
      description: "Salão de Festas",
      reference: "home-lazer",
    },
    {
      src: "/images/lazer/pet-place.jpg",
      description: "Pet Place",
      reference: "home-lazer",
    },
    {
      src: "/images/lazer/playground.jpg",
      description: "Playground",
      reference: "home-lazer",
    },
  ];

  return (
    <Section>
      <Container>
        <Title>
          <h2>Quer ser feliz?</h2>
          <h2>Bem-vindo ao Clube.</h2>
          <p>Infraestrutura completa para o seu lazer.</p>
        </Title>
      </Container>
      <SplideCarousel
        nameSlide="lazer"
        items={items}
        description={true}
        magnifying={false}
      />
      <Button onClick={() => router.push("/lazer")}>saiba mais</Button>
    </Section>
  );
}
