import styled from "styled-components";

export const Section = styled.section`
  margin-top: 7.5rem;
  margin-bottom: 8rem;

  .containerRight {
    padding-left: 7rem;

    @media (max-width: 1280px) {
      padding-left: 3rem;
    }

    @media (max-width: 1023px) {
      padding: 0 2rem;
      margin-top: 2rem;
    }
  }
`;

export const ContainerInformation = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: ${({ theme }) => theme.grena};
  padding: 5rem;
`;

export const Logo = styled.div`
  margin-bottom: 3rem;

  img {
    width: 35%;
    max-width: 100%;
  }
`;

export const Information = styled.div`
  h4 {
    font-size: 1.2rem;
    font-family: "Gotham Light";
    font-weight: 500;
    width: 55%;
    color: ${({ theme }) => theme.white};
    margin-bottom: 2rem;

    @media (max-width: 1280px) {
      width: 60%;
    }

    @media (max-width: 767px) {
      width: 100%;
    }
  }
`;

export const Box = styled.div`
  margin-bottom: 1rem;
  a {
    display: flex;
    text-decoration: none;
    color: ${({ theme }) => theme.white};
  }

  span {
    font-size: 1.2rem;
  }
`;

export const Image = styled.div`
  margin-right: 1rem;
`;

export const Title = styled.div`
  h2 {
    font-family: "Gotham Medium";
    font-size: 4.3rem;

    @media (max-width: 1280px) {
      font-size: 4rem;
    }

    @media (max-width: 767px) {
      font-size: 3.4rem;
    }
  }

  span {
    font-family: "Bodoni MT";
    font-weight: 500;
    font-size: 4.5rem;
    color: ${({ theme }) => theme.grena};
    margin-left: 13rem;

    @media (max-width: 1280px) {
      font-size: 4.1rem;
      margin-left: 12rem;
    }

    @media (max-width: 767px) {
      font-size: 3.9rem;
      margin-left: 0;
    }
  }
`;

export const ContainerForm = styled.div`
  button {
    text-align: center;
    margin-left: 1rem;
    margin-top: 1.5rem;

    background: transparent;
    border: none;
    color: ${({ theme }) => theme.black};
    text-transform: uppercase;
    font-family: "Gotham Medium";
    font-size: 1.3rem;
    position: relative;
    outline: 0;
    transition: color 0.3s;

    &::before {
      position: absolute;
      content: "";
      background: url(/images/contato/colcheteLeft.svg);
      width: 9px;
      height: 51px;
      left: -1rem;
      top: -1rem;
    }

    &::after {
      position: absolute;
      content: "";
      background: url(/images/contato/colcheteRight.svg);
      width: 10px;
      height: 50px;
      right: -1rem;
      top: -1rem;
    }

    &:hover {
      color: ${({ theme }) => theme.grena};
    }

    &:disabled {
      opacity: 0.6;
      cursor: not-allowed;
    }
  }
`;

export const ContainerPolitica = styled.div`
  display: flex;
  align-items: center;
  margin-top: 0.8rem;

  label {
    margin: 0;

    span {
      padding-left: 0;
      color: ${({ theme }) => theme.grena};
    }

    .Mui-checked {
      color: ${({ theme }) => theme.grena};
    }
  }

  > span {
    margin-left: 0.6rem;
    color: ${({ theme }) => theme.black};

    strong {
      cursor: pointer;
    }

    @media (max-width: 767px) {
      font-size: 0.9rem;
    }
  }
`;
