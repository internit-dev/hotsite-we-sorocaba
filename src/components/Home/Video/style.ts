import styled from "styled-components";

export const Section = styled.section`
    margin: 5rem 0;
`;

export const ContainerVideo = styled.div`
    iframe{
        width: 100%;
        height: 850px;

        @media (max-width: 767px){
            height: 480px;
        }
    }
`;