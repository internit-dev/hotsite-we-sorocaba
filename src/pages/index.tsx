import type { NextPage } from "next";
import Head from "next/head";
import Banner from "../components/Banner";
import Botafogo from "../components/Home/Botafogo";
import { Exclusivo } from "../components/Home/Exclusivo";
import Lazer from "../components/Home/Lazer";
import Planta from "../components/Home/Planta";
import Video from "../components/Home/Video";
import Footer from '../components/Footer';
import Header from '../components/Header';
import Script from 'next/script'

const Home: NextPage = () => {

  return (
    <>
      <Head>
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-182152877-4"></script>
      <script dangerouslySetInnerHTML={{
          __html: `
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'UA-182152877-4');`,}}>

        </script>
  
        <title>Apartamento à venda em Botafogo | 3 e 4 quartos | GAFISA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Site da imobiliária performance" />
        <meta
          name="description"
          content="Receba mais informações sobre o lançamento da Gafisa no bairro de Botafogo, Rio de Janeiro. Clique e saiba mais."
        />
        <meta name="author" content="Internit" />
        <meta name="keywords" content="we sorocaba, gafisa, apartamento botafogo" />

          <script type="text/javascript" src="scripts/gtagHead.js"></script>

      </Head>
      <Header showLogo={false} />

      <Banner page="Home" />
      <Exclusivo />
      <Video />
      <Planta />
      <Lazer />
      <Botafogo />
      <Footer showLogos={true} information={true} />
    </>
  );
};

export default Home;
