import styled from "styled-components";

export const Section = styled.section`
  margin-top: 3rem;
  text-align: center;

  .swiper-slide {
    img {
      border-radius: 1rem;
      border-radius: 1rem;
      height: 405px;
      object-fit: cover;
      object-position: center;
    }
  }

  .prev {
    left: 38rem !important;
    svg {
      width: 3rem !important;
      height: 3rem !important;
    }

    @media (min-width: 1280px) and (max-width: 1919px) {
      left: 28rem !important;
    }
    @media (max-width: 1279px) {
      left: 22rem !important;
    }

    @media (max-width: 1023px) {
      left: 17rem !important;
    }

    @media (max-width: 767px) {
      left: 1rem !important;
    }
  }

  .next {
    right: 38rem !important;

    svg {
      width: 3rem !important;
      height: 3rem !important;
    }

    @media (min-width: 1280px) and (max-width: 1919px) {
      right: 28rem !important;
    }
    @media (max-width: 1279px) {
      right: 22rem !important;
    }

    @media (max-width: 1023px) {
      right: 17rem !important;
    }

    @media (max-width: 767px) {
      right: 1rem !important;
    }
  }

  @media (max-width: 767px) {
    margin-top: 6rem;
  }
`;

export const Title = styled.div`
  text-align: left;
  h2 {
    font-family: "Gotham Medium";
    font-size: 3.3rem;

    &:not(:first-child) {
      margin-bottom: 1.3rem;
    }

    @media (max-width: 767px) {
      font-size: 2.5rem;
    }
  }

  p {
    font-size: 1.4rem;
    font-family: "Gotham Light";
    font-weight: 600;
  }
`;
