import { createContext, ReactNode, useContext, useState } from "react";

interface ISidebarContext {
    isSideBarOpen: boolean;
    handleSideBar(state: boolean): void;
}

interface ToggleSideBarContextProps {
  children: ReactNode;
}

export const ToggleSideBar = createContext<ISidebarContext>(
  {} as ISidebarContext
);

export const ToggleSideBarContext = ({
  children,
}: ToggleSideBarContextProps) => {
  const [isSideBarOpen, setIsSideBarOpen] = useState(false);

  const handleSideBar = (status: boolean) => {
    if (status) {
      setIsSideBarOpen(true);
    } else {
      setIsSideBarOpen(false);
    }
  };
  return(
      <ToggleSideBar.Provider value={{ isSideBarOpen, handleSideBar }}>
          {children}
      </ToggleSideBar.Provider>
  )
};

export const useSideBar = () => useContext(ToggleSideBar);