import styled from "styled-components";

export const Section = styled.section`
  margin: 4rem 0;
`;

export const ContainerTitle = styled.div`
  text-align: center;
  margin-bottom: 5rem;
  img {
    width: 18%;
    margin-bottom: 2rem;
  }

  h3 {
    font-family: "Gotham Light";
    font-size: 1.5rem;
    font-weight: 600;
  }

  h2 {
    font-family: "Gotham Medium";
    font-size: 3rem;
  }

  p {
    width: 54%;
    margin: 2rem auto 0 auto;
    font-size: 1.2rem;
    line-height: 24px;
    font-family: "Gotham Light";
    font-weight: 600;

    @media (max-width: 1280px){
      width: 78%;
    }
  }
`;

export const ContainerInformation = styled.div`
  .firstContainerLeft,
  .secondContainerRight {
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 2rem;
    > div {
      width: 75%;
      margin: 0 auto;

      @media (max-width: 767px) {
        width: 100%;
      }
    }
  }

  .firstContainerRight {
    position: relative;

    &::after {
      position: absolute;
      content: "";
      background: url(/images/brown-bracket-right2.png);
      width: 98px;
      height: 539px;
      bottom: -6rem;
      right: 2rem;

      @media (max-width: 1280px) {
        width: 66px;
        height: 363px;
        bottom: -4rem;
        background-size: contain;
      }

      @media (max-width: 1023px) {
        width: 45px;
        height: 249px;
        right: 1rem;
      }
    }

    @media (max-width: 1023px) {
      order: 1;
    }
  }

  .firstContainerLeft {
    @media (max-width: 1023px) {
      order: 2;
    }
  }

  .secondContainerRight {
    h3 {
      font-size: 3.1rem;
      margin-top: 2rem;

      @media (max-width: 767px) {
        font-size: 2.6rem;
      }
    }

    @media (max-width: 1023px) {
      order: 4;
    }
  }

  .secondContainerLeft {
    position: relative;

    &::before {
      position: absolute;
      content: "";
      background: url(/images/brown-bracket-left2.png);
      width: 98px;
      height: 539px;
      top: -4rem;
      left: 2rem;

      @media (max-width: 1280px) {
        width: 65px;
        height: 357px;
        top: -3rem;
        background-size: contain;
      }

      @media (max-width: 1023px) {
        width: 45px;
        height: 249px;
        top: -1rem;
      }
    }

    @media (max-width: 1023px) {
      order: 3;
    }
  }
`;

export const ContainerText = styled.div`
  h3 {
    font-family: "Bodoni MT";
    font-size: 5rem;
    margin-bottom: 2rem;
  }

  p {
    font-family: "Gotham Light";
    font-weight: 600;
    font-size: 1rem;

    & + p {
      margin-top: 1.3rem;
    }
  }
`;
