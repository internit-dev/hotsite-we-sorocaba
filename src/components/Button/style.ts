import styled from "styled-components";

export const Container = styled.button`
  text-align: center;
  margin-top: 3rem;

  background: transparent;
  border: none;
  color: #6A072F;
  text-transform: uppercase;
  font-family: "Gotham Medium";
  font-size: 1rem;
  position: relative;
  outline: 0;
  transition: color .3s;

  &::before {
    position: absolute;
    content: "";
    background: url(/images/home/colcheteLeft.png);
    width: 9px;
    height: 51px;
    left: -1rem;
    top: -1rem;
  }

  &::after {
    position: absolute;
    content: "";
    background: url(/images/home/colcheteRight.png);
    width: 10px;
    height: 50px;
    right: -1rem;
    top: -1rem;
  }

  &:hover{
    color: ${({ theme }) => theme.green};
  }
`;
