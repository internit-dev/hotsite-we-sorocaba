import styled, { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
    @font-face {
        font-family: 'Gotham Regular';
        src: url(/fonts/Gotham/Gotham-Book.otf);
    }

    @font-face {
        font-family: 'Gotham Light';
        src: url(/fonts/Gotham/Gotham-Light.otf);
    }

    @font-face {
        font-family: 'Gotham Medium';
        src: url(/fonts/Gotham/Gotham-Medium.otf);
    }

    @font-face {
        font-family: 'Gotham Bold';
        src: url(/fonts/Gotham/Gotham-Bold.ttf);
    }

    @font-face{
        font-family: 'Bodoni MT';
        src: url(/fonts/Bodoni/BodoniMT.ttf);
    }

    * {
      margin: 0;
      padding: 0;
      outline: none;
      box-sizing: border-box;
    }

    @media (max-width: 1080px){
        html{
            font-size: 93.75%;
        }
    }

    @media (max-width: 720px){
        html{
            font-size: 87.5%;
        }
    }

    body{
        &::-webkit-scrollbar {
        width: 8px;              
        }

        &::-webkit-scrollbar-track {
        background: ${({ theme }) => theme.ice};      
        }

        &::-webkit-scrollbar-thumb {
        background-color: ${({ theme }) => theme.grena};      
        border: 1px solid ${({ theme }) => theme.grena};  
        }
    }

    body, input, button {
      font-size: 16px;
      font-family: 'Gotham Regular';
      color: ${({ theme }) => theme.black};
    }

    h1, h2, h3, h4, h5{
      font-family: 'Gotham Regular';
      font-weight: 400;
      color: ${({ theme }) => theme.black};
    }
    
    button { cursor: pointer; font-weight: 500; }
    
`;

export const Container = styled.div`
  max-width: 1595px;
  margin: 0 auto;
  padding: 0 1rem;
`;
