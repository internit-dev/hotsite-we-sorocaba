import { Grid } from "@mui/material";

import Banner from "../../components/Banner";
import Header from "../../components/Header";
import Footer from "../../components/Footer";

import { Container } from "../../styles/global";
import { Section, ContainerImage, ContainerGafisa, Text } from "../../styles/pages/realizacao/style";
import { ContainerTitle } from "../../styles/pages/viverbem/style";
import Head from "next/head";

export default function Realizacao() {
  return (
    <>
      <Head>
        <title>Apartamento à venda em Botafogo | 3 e 4 quartos | GAFISA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Site da imobiliária performance" />
        <meta
          name="description"
          content="Receba mais informações sobre o lançamento da Gafisa no bairro de Botafogo, Rio de Janeiro. Clique e saiba mais."
        />
        <meta name="author" content="Internit" />
        <meta
          name="keywords"
          content="we sorocaba, gafisa, apartamento botafogo"
        />
      </Head>
      <Header showLogo={true} />
      <Banner page="Realização" />
      <Section>
        <Container>
          <ContainerTitle>
            <img src="/images/logo-gafisa.png" width="20%" alt="Gafisa" />
            <h3>Mais que empreendimentos,</h3>
            <h2>construímos uma cidade.</h2>
            <p>
              Se juntássemos todos os empreendimentos já construídos pela
              Gafisa, teríamos uma cidade inteira com mais de 1 milhão e meio de
              habitantes.
            </p>
          </ContainerTitle>
          <ContainerImage>
            <img
              src="/images/realizacao/predios.png"
              width="100%"
              alt="Prédios construídos pela Gafisa"
            />
            <span>
              Todos os prédios nessa imagem foram construídos pela gafisa
            </span>
          </ContainerImage>
          <ContainerGafisa>
            <Grid container spacing={2}>
              <Grid item md={6} xs={12} className="containerLeft">
                <Text>
                  <h2>Conheça a Gafisa</h2>
                  <p>
                    Somos movidos pela força do nosso triângulo. Centrados em
                    nossos clientes, trazemos inspiração e inovação para dar
                    vida aos empreendimentos que construímos e entregarmos cada
                    vez mais sonhos para que as pessoas vivam melhor. Somos a
                    Gafisa, uma das maiores e mais respeitadas incorporadoras e
                    construtoras do país.
                  </p>
                  <p>
                    Nossa marca é reconhecida pela solidez, qualidade, tradição,
                    credibilidade e inovação em cada projeto desenvolvido e
                    entregue. Esses pilares fizeram nossa Companhia conquistar o
                    respeito dos clientes e do mercado imobiliário.
                  </p>
                  <p>
                    Temos compromisso com nossos clientes e foco em crescimento
                    e inovação para levar o melhor espaço para viver com
                    conforto, bem-estar e segurança para um número cada vez
                    maior de pessoas. Toda nossa história e pioneirismo refletem
                    em nossos números e reconhecimentos.
                  </p>
                  <p>
                    Transformamos projetos em experiências e expectativas em
                    satisfação para nossos clientes e acionistas. Porque mais do
                    que entregar imóveis, trazemos novas ideias e soluções, seja
                    em um projeto totalmente diferenciado ou no detalhe de um
                    acabamento. Para nós da Gafisa, cada cliente é único e pode
                    ter um Gafisa do seu jeito para morar, trabalhar ou
                    investir.
                  </p>
                </Text>
              </Grid>
              <Grid item md={6} xs={12} className="containerRight">
                <img
                  src="/images/realizacao/image-1.svg"
                  width="100%"
                  alt="Sala de estar"
                />
              </Grid>
              <Grid item xs={12} mt={6} className="containerVisite">
                <a href="https://www.gafisa.com.br/" target="_blank" rel="noreferrer">
                  <div>
                    <h5>Visite nosso site</h5>
                    <p>www.gafisa.com.br</p>
                  </div>
                </a>
              </Grid>
            </Grid>
          </ContainerGafisa>
        </Container>
      </Section>
      <Footer showLogos={false} information={false} />
    </>
  );
}
