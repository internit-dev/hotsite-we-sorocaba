import { useRouter } from "next/router";
import Head from "next/head";
import { Grid } from "@mui/material";

import Banner from "../../components/Banner";
import Header from "../../components/Header";
import Footer from "../../components/Footer";

import { Container } from "../../styles/global";

import {
  Section,
  ContainerHeader,
  ContainerImages,
  ContainerEscritorios,
  Project,
} from "../../styles/pages/empreendimento/style";
import { Button } from "../../components/Button";

export default function Empreendimento() {
  const router = useRouter();
  return (
    <>
      <Head>
        <title>Apartamento à venda em Botafogo | 3 e 4 quartos | GAFISA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Site da imobiliária performance" />
        <meta
          name="description"
          content="Receba mais informações sobre o lançamento da Gafisa no bairro de Botafogo, Rio de Janeiro. Clique e saiba mais."
        />
        <meta name="author" content="Internit" />
        <meta
          name="keywords"
          content="we sorocaba, gafisa, apartamento botafogo"
        />
      </Head>
      <Header showLogo={true} />
      <Banner page="Empreendimento" />
      <Section>
        <Container>
          <ContainerHeader>
            <div className="title">
              <h2>We live, we work, we play</h2>
              <h6>We em todos os momentos</h6>
            </div>
            <div className="subtitle">
              <p>
                Aprecie as mais variadas exposições, filmes e peças. Aproveite os mais
                variados serviços. Experimente os mais variados sabores.
                Botafogo nos representa.
              </p>
            </div>
          </ContainerHeader>
          <ContainerImages>
            <Grid container spacing={2}>
              <Grid item sm={6} xs={12} className="containerLeft">
                <div className="first-image">
                  <img
                    src="/images/empreendimento/vistaFrontal.png"
                    alt="Vista Frontal"
                    width="100%"
                  />
                  <span>Vista frontal</span>
                </div>
                <div className="second-image">
                  <img
                    src="/images/empreendimento/fachadaNoturna.png"
                    alt="Fachada Noturna"
                    width="100%"
                  />
                  <span>Fachada noturna</span>
                </div>
              </Grid>
              <Grid item sm={6} xs={12} className="containerRight">
                <div className="first-image">
                  <img
                    src="/images/empreendimento/coworking.png"
                    alt="Coworking"
                    width="100%"
                  />
                  <span>Coworking</span>
                </div>
                <div className="second-image">
                  <img
                    src="/images/empreendimento/familia.png"
                    alt="Familia"
                    width="100%"
                  />
                </div>
              </Grid>
            </Grid>
          </ContainerImages>
          <ContainerEscritorios>
            <ContainerHeader>
              <div className="title">
                <h2>4 escritórios em sintonia</h2>
                <h6>Por um projeto único</h6>
              </div>
              <div className="subtitle">
                <p>
                  Aprecie as mais variadas exposições, filmes e peças. Aproveite os mais
                  variados serviços. Experimente os mais variados sabores.
                  Botafogo nos representa.
                </p>
              </div>
            </ContainerHeader>
            <Grid container spacing={2} marginTop={5} className="container">
              <Grid item sm={6} xs={12} className="containerLeft">
                <Project className="first">
                  <div className="image">
                    <img
                      src="/images/empreendimento/larqTurano.png"
                      width="100%"
                      alt="Larq Turano"
                    />
                  </div>
                  <div className="logo">
                    <img
                      src="/images/empreendimento/logoLarqTurano.png"
                      width="100%"
                      alt="Logo Larq Turano"
                    />
                  </div>
                  <div className="description">
                    <span>Projeto de arquitetura</span>
                    <p>
                      “Esse projeto é a consequência natural e irreversível de
                      um novo conceito de residência que, cada vez mais,
                      solicita soluções abrangentes e funcionais. Compactas, mas
                      completas; possibilitando alternâncias de uso sem abrir
                      mão do conforto.”
                    </p>
                  </div>
                </Project>
                <Project>
                  <div className="image">
                    <img
                      src="/images/empreendimento/saAlmeida.png"
                      width="100%"
                      alt="Sa Almeida"
                    />
                  </div>
                  <div className="logo">
                    <img
                      src="/images/empreendimento/logoSaAlmeida.png"
                      width="100%"
                      alt="Logo Sa Almeida"
                    />
                  </div>
                  <div className="description">
                    <span>Projeto de paisagismo</span>
                    <p>
                      “O paisagismo cria a possibilidade de um novo olhar sobre
                      as plantas, responsáveis pela manutenção de todas as
                      demais formas de vida do planeta. Inserir vida e poesia no
                      ambiente construído e ressignificar a matéria inerte das
                      edificações é uma das mis sões do paisagismo, criando um
                      ambiente especial para cada um, mas sempre pensando em
                      todos nós.”
                    </p>
                  </div>
                </Project>
              </Grid>
              <Grid item sm={6} xs={12} className="containerRight">
                <Project>
                  <div className="image">
                    <img
                      src="/images/empreendimento/feuArquitetura.png"
                      width="100%"
                      alt="Feu Arquitetura"
                    />
                  </div>
                  <div className="logo">
                    <img
                      src="/images/empreendimento/logoFeuArquitetura.png"
                      width="100%"
                      alt="Logo Feu Arquitetura"
                    />
                  </div>
                  <div className="description">
                    <span>Projeto de fachada</span>
                    <p>
                      “Janelas, molduras, portais. No conceito dessa fachada
                      criado pela FEU Arquitetura, esses símbolos se transformam
                      em formas que dão identidade ao edifício.”
                    </p>
                  </div>
                </Project>
                <Project className="last">
                  <div className="image">
                    <img
                      src="/images/empreendimento/izabelaFernanda.png"
                      width="100%"
                      alt="Larq Turano"
                    />
                  </div>
                  <div className="logo">
                    <img
                      src="/images/empreendimento/logoIzabelaFernanda.png"
                      width="100%"
                      alt="Logo Larq Turano"
                    />
                  </div>
                  <div className="description">
                    <span>Projeto de fachada</span>
                    <p>
                      “Tivemos o privilégio de participar deste empreendimento
                      que entende que cada vez mais as pessoas procuram um lar
                      que acolha e proporcione os melhores sentimentos.
                      Entendemos que um lar é construído de emoções e pensamos
                      em cada detalhe para facilitar o encontro com o tão
                      desejado bem es tar.”
                    </p>
                  </div>
                </Project>
              </Grid>
            </Grid>
          </ContainerEscritorios>
          <Button onClick={() => router.push('/contato')}>solicite mais informações</Button>
        </Container>
      </Section>
      <Footer showLogos={true} information={false} />
    </>
  );
}
