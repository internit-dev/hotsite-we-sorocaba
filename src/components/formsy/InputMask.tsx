import ReactInputMask from "react-input-mask";
import { TextField } from "@mui/material";
import { styled } from "@mui/material/styles";
import { withFormsy } from "formsy-react";
import React from "react";

const TextFieldCustomized = styled(TextField)({
	"& .MuiInput-root:before": {
		borderColor: "#8F392A",
	},
	"& .MuiInput-root:after": {
		borderColor: "#8F392A",
	},
	"& .MuiInput-root:hover:not(.Mui-disabled):before": {
		borderColor: "#8F392A",
	},
	"& label": {
		color: "#050706",
		fontFamily: "Work Sans, sans-serif",
	},
	"& .Mui-focused": {
		color: "#8F392A !important",
	},
});

function InputMask(props: any) {
	const { errorMessage } = props;
	const value = props.value || "";

	function changeValue(event: any) {
		props.setValue(event.currentTarget.value);
		if (props.onChange) {
			props.onChange(event);
		}
	}

	return (
		<ReactInputMask
			{...props}
			onChange={changeValue}
			value={value}
			error={Boolean(
				(!props.isPristine && props.showRequired) || errorMessage
			)}
			helperText={errorMessage}
		>
			{() => <TextFieldCustomized {...props} style={{ width: "100%" }} />}
		</ReactInputMask>
	);
}

export default React.memo(withFormsy(InputMask));
