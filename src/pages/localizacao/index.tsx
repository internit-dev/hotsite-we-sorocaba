import { useRouter } from "next/router";
import Head from "next/head";

import Banner from "../../components/Banner";
import Header from "../../components/Header";
import Footer from "../../components/Footer";

import { Container } from "../../styles/global";

import {
  Section,
  ContainerHeader,
  Title,
  ContainerImage,
  ContainerLocation,
  ContainerInformation,
  Information,
  ContainerWeLive,
  WeLiveTitle,
  WeLiveSubTitle,
  WeLiveImages,
  Text,
  ContainerWeLiveTitle,
} from "../../styles/pages/localizacao/style";
import { Grid } from "@mui/material";
import { Button } from "../../components/Button";

export default function Localizacao() {
  const router = useRouter();
  const locations = [
    { name: "Shopping Rio Sul", distance: "3,2km" },
    { name: "Metrô", distance: "750m" },
    { name: "Jardim Botânico", distance: "2,8km" },
    { name: "Hospital", distance: "1,7km" },
    { name: "Praia de Copacabana", distance: "2,8km" },
    { name: "Centro da Cidade", distance: "7,8km" },
  ];

  return (
    <>
      <Head>
        <title>Apartamento à venda em Botafogo | 3 e 4 quartos | GAFISA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Site da imobiliária performance" />
        <meta
          name="description"
          content="Receba mais informações sobre o lançamento da Gafisa no bairro de Botafogo, Rio de Janeiro. Clique e saiba mais."
        />
        <meta name="author" content="Internit" />
        <meta
          name="keywords"
          content="we sorocaba, gafisa, apartamento botafogo"
        />
      </Head>
      <Header showLogo={true} />
      <Banner page="Localização" />
      <Section>
        <Container>
          <ContainerHeader>
            <Title>
              <h2>Botafogo</h2>
              <p>
                Um bairro que valoriza o{" "}
                <span>
                  <strong>&quot;</strong>nós<strong>&quot;</strong>
                </span>
              </p>
            </Title>
          </ContainerHeader>
          <ContainerImage>
            <img
              src="/images/localizacao/botafogo.svg"
              width="100%"
              alt="Bairro de Botafogo"
            />
          </ContainerImage>
          <ContainerLocation>
            <h6>Rua Socoraba, 701</h6>
            <Grid container spacing={2}>
              <Grid item md={6} xs={12}>
                <img
                  src="/images/localizacao/mapa.png"
                  width="100%"
                  alt="Mapa do local"
                />
              </Grid>
              <Grid item md={6} xs={12} className="containerRight">
                <ContainerInformation>
                  {locations.map((location, index) => (
                    <Information key={`location-${index}`}>
                      <p>{location.name}</p>
                      <p>{location.distance}</p>
                    </Information>
                  ))}
                </ContainerInformation>
                <Button onClick={() => router.push('/contato')}>solicite mais informações</Button>
              </Grid>
            </Grid>
          </ContainerLocation>
          <ContainerWeLive>
            <ContainerWeLiveTitle>
              <WeLiveTitle>
                <h2>we live.</h2>
                <h2>we drink.</h2>
                <h2>we eat.</h2>
              </WeLiveTitle>
              <WeLiveSubTitle>
                <h6>
                  Para quem gosta de arte, cultura, lazer, gastronomia, noite e
                  dia.
                </h6>
              </WeLiveSubTitle>
            </ContainerWeLiveTitle>
            <WeLiveImages>
              <Grid container spacing={2}>
                <Grid item md={6} xs={12} className="containerImagesLeft">
                  <div>
                    <img
                      src="/images/localizacao/comida.svg"
                      width="100%"
                      alt="Prato de comida"
                    />
                  </div>
                  <div>
                    <img
                      src="/images/localizacao/pessoas.svg"
                      width="100%"
                      alt="Pessoas"
                    />
                  </div>
                </Grid>
                <Grid item md={6} xs={12} className="containerImagesRight">
                  <div>
                    <img
                      src="/images/localizacao/familia.svg"
                      width="100%"
                      alt="Família"
                    />
                  </div>
                  <Text>
                    <h3>we buy.</h3>
                    <h3>we study.</h3>
                    <h3>we work.</h3>
                  </Text>
                </Grid>
              </Grid>
            </WeLiveImages>
          </ContainerWeLive>
        </Container>
      </Section>
      <Footer showLogos={true} information={false} />
    </>
  );
}
