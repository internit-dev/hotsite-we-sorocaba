import { Section, ContainerVideo } from './style';

export default function Video() {
  return (
    <Section>
      <ContainerVideo>
        <iframe
          width="560"
          height="315"
          src="https://my.matterport.com/show/?m=2K5M7naj8ik&play=1&sr=-.01,-.88&ss=4"
          title="YouTube video player"
          frameBorder="0"
          allowFullScreen
        ></iframe>
      </ContainerVideo>
    </Section>
  );
}
