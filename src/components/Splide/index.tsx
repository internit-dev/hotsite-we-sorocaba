import { useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation } from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import FsLightbox from "fslightbox-react";
import { ContainerSlide, Carousel } from "./style";

interface SplideCarouselProps {
  items: Array<ItemsProps>;
  description: boolean;
  nameSlide: string;
  magnifying: boolean;
}

interface ItemsProps {
  src: string;
  description?: string;
  reference: string;
}

export function SplideCarousel({
  items,
  description,
  nameSlide,
  magnifying
}: SplideCarouselProps) {
  const [lightboxControllerImages, setLightboxControllerImages] = useState({
    toggler: false,
    slide: 1,
  });

  return (
    <ContainerSlide>
      <Carousel>
        <Swiper
          spaceBetween={80}
          slidesPerView={1}
          breakpoints={{
            767: {
              slidesPerView: 2,
              spaceBetween: 40,
            },
            1024: {
              slidesPerView: 3,
            },
            1280: {
              slidesPerView: 3,
              spaceBetween: 70,
            },
            1680: {
              slidesPerView: 3,
              spaceBetween: 80,
            },
          }}
          navigation={{
            prevEl: `.prev-${nameSlide}`,
            nextEl: `.next-${nameSlide}`,
          }}
          modules={[Navigation]}
        >
          <div
            className={`prev-${nameSlide} prev`}
          >
            {nameSlide === "lazer" ? (
              <svg
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M12 2C6.486 2 2 6.486 2 12C2 17.514 6.486 22 12 22C17.514 22 22 17.514 22 12C22 6.486 17.514 2 12 2ZM12 20C7.589 20 4 16.411 4 12C4 7.589 7.589 4 12 4C16.411 4 20 7.589 20 12C20 16.411 16.411 20 12 20Z"
                  fill="white"
                />
                <path
                  d="M13.2929 6.29297L7.58594 12L13.2929 17.707L14.7069 16.293L10.4139 12L14.7069 7.70697L13.2929 6.29297Z"
                  fill="white"
                />
              </svg>
            ) : (
              <svg
                width="309"
                height="309"
                viewBox="0 0 109 109"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <circle cx="54.5" cy="54.5" r="54.5" fill="#8F392A" />
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M61.589 35.4519C61.7153 35.5948 61.8154 35.7646 61.8838 35.9515C61.9522 36.1383 61.9873 36.3387 61.9873 36.541C61.9873 36.7434 61.9522 36.9437 61.8838 37.1306C61.8154 37.3175 61.7153 37.4872 61.589 37.6301L46.2755 55.0004L61.589 72.3707C61.8436 72.6595 61.9866 73.0513 61.9866 73.4598C61.9866 73.8683 61.8436 74.26 61.589 74.5489C61.3344 74.8377 60.9891 75 60.629 75C60.269 75 59.9237 74.8377 59.6691 74.5489L43.3983 56.0895C43.2721 55.9466 43.1719 55.7769 43.1035 55.59C43.0352 55.4031 43 55.2027 43 55.0004C43 54.7981 43.0352 54.5977 43.1035 54.4108C43.1719 54.2239 43.2721 54.0542 43.3983 53.9113L59.6691 35.4519C59.795 35.3087 59.9446 35.195 60.1094 35.1175C60.2741 35.0399 60.4507 35 60.629 35C60.8074 35 60.984 35.0399 61.1487 35.1175C61.3134 35.195 61.463 35.3087 61.589 35.4519Z"
                  fill="white"
                />
              </svg>
            )}
          </div>
          {items.map((slide, index) => (
            <SwiperSlide
              key={`${slide.reference}-${index}`}
              onClick={() =>
                setLightboxControllerImages({
                  toggler: !lightboxControllerImages.toggler,
                  slide: index + 1,
                })
              }
            >
              <img src={slide.src} width="100%" alt={slide.description} />
              {magnifying && (
                <div className="magnifying">
                  <img src="/images/home/icons/lupa.png" width="20%" alt="Lupa" />
                </div>
              )}
              {description && <span>{slide.description}</span>}
            </SwiperSlide>
          ))}
          <div className={`next-${nameSlide} next`}>
            {nameSlide === "lazer" ? (
              <svg
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M12 2C6.486 2 2 6.486 2 12C2 17.514 6.486 22 12 22C17.514 22 22 17.514 22 12C22 6.486 17.514 2 12 2ZM12 20C7.589 20 4 16.411 4 12C4 7.589 7.589 4 12 4C16.411 4 20 7.589 20 12C20 16.411 16.411 20 12 20Z"
                  fill="white"
                />
                <path
                  d="M9.29297 7.70697L13.586 12L9.29297 16.293L10.707 17.707L16.414 12L10.707 6.29297L9.29297 7.70697Z"
                  fill="white"
                />
              </svg>
            ) : (
              <svg
                width="309"
                height="309"
                viewBox="0 0 109 109"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <circle cx="54.5" cy="54.5" r="54.5" fill="#8F392A" />
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M47.3983 35.4519C47.5243 35.3087 47.6739 35.195 47.8386 35.1175C48.0034 35.0399 48.18 35 48.3583 35C48.5367 35 48.7133 35.0399 48.878 35.1175C49.0427 35.195 49.1923 35.3087 49.3183 35.4519L65.589 53.9113C65.7153 54.0542 65.8154 54.2239 65.8838 54.4108C65.9522 54.5977 65.9873 54.7981 65.9873 55.0004C65.9873 55.2027 65.9522 55.4031 65.8838 55.59C65.8154 55.7769 65.7153 55.9466 65.589 56.0895L49.3183 74.5489C49.0637 74.8377 48.7184 75 48.3583 75C47.9983 75 47.6529 74.8377 47.3983 74.5489C47.1437 74.26 47.0007 73.8683 47.0007 73.4598C47.0007 73.0513 47.1437 72.6595 47.3983 72.3707L62.7118 55.0004L47.3983 37.6301C47.2721 37.4872 47.1719 37.3175 47.1035 37.1306C47.0352 36.9437 47 36.7434 47 36.541C47 36.3387 47.0352 36.1383 47.1035 35.9515C47.1719 35.7646 47.2721 35.5948 47.3983 35.4519Z"
                  fill="white"
                />
              </svg>
            )}
          </div>
        </Swiper>
      </Carousel>
      <FsLightbox
        toggler={lightboxControllerImages.toggler}
        sources={items.map((item) => item.src)}
        slide={lightboxControllerImages.slide}
      />
    </ContainerSlide>
  );
}
